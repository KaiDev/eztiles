#EzTiles
An app to edit the visual appearance of the Windows 10 Startmenu.

##Used software

####PriTools
Copyright 2016 Christoph Hausner (Apache License, Version 2.0)  
https://github.com/chausner/PriTools

####Windows-API-Code-Pack-Core and Windows-API-Code-Pack-Shell 
by Microsoft?  
https://github.com/aybe/Windows-API-Code-Pack-1.1

####QuickConverter
Copyright (c) 2017 JohannesMoersch (MIT License)  
https://github.com/JohannesMoersch/QuickConverter

####moldinium
Copyright (c) 2016 Jens Theisen (MIT License)  
https://github.com/jtheisen/moldinium

####AutoDependencyProperty.Fody
Copyright (c) 2012 Simon Cropp and contributors (MIT License)  
https://bitbucket.org/robertvazan/autodependencyproperty.fody/src/default/

####Equals.Fody 
Copyright (c) 2013 Rafa³ Jasica and contributors (MIT License)  
https://github.com/Fody/Equals

####Fody
Copyright (c) Simon Cropp and contributors (MIT License)  
https://github.com/Fody/Fody

####MahApps.Metro
Copyright (c) 2016 MahApps (MIT License)  
https://github.com/MahApps/MahApps.Metro

####ControlzEx
Copyright (c) 2015 Jan Karger, Bastian Schmidt (MIT License)  
https://github.com/ControlzEx/ControlzEx

####Castle.Core
Copyright 2004-2016 Castle Project - http://www.castleproject.org/ (Apache License, Version 2.0)  
https://github.com/castleproject/Core