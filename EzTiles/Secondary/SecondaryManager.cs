﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Windows.ApplicationModel;
using Windows.Management.Deployment;
using EzTiles.Data;
using EzTiles.Util;
using EzTiles_Common;
using MahApps.Metro.Controls;

namespace EzTiles
{
//https://docs.microsoft.com/de-de/windows/configuration/start-layout-xml-desktop#startsecondarytile
    public static class SecondaryManager
    {
        public const string NAME = "8460acbe-01ad-4d02-ba8d-c37e3a0f7f68";

        public static readonly bool IsHelperInstalled;

        public static readonly string Aumid;

        public static readonly Package HelperPackage;

        public static readonly DirectoryInfo HelperFolder;

        static SecondaryManager()
        {
            var packageManager = new PackageManager();
            var packages = packageManager.FindPackagesForUser("").ToList();
            HelperPackage = packages.SingleOrDefault(p => p.Id.Name == NAME);

            if (HelperPackage != null)
            {
                IsHelperInstalled = true;
                Aumid = HelperPackage.Id.FamilyName + "!App";
                HelperFolder = new DirectoryInfo(Environment.ExpandEnvironmentVariables(
                    @"%LOCALAPPDATA%\Packages\" + HelperPackage.Id.FamilyName + @"\LocalState"));
                if (!HelperFolder.Exists) HelperFolder.Create();
            }
        }

        public static async Task<StartmenuSecondaryHelperTile> CreateNewSecondary(
            string name, 
            string arguments, 
            Color backgroundColor,
            ForegroundText foregroundText,
            ShowNameOnSquare150x150Logo showNameOnSquare150X150Logo,
            VisualManifest visualManifest = null)
        {
            ThrowIfHelperMissing();

            var guid = Guid.NewGuid().ToString();
            
            arguments = HelperAction.EscapeSingleArgument(arguments);
            
            var cmdLine = HelperAction.ComposeArguments(nameof(IHelperHandler.CreateNewSecondary), 
                guid, 
                name, 
                arguments, 
                backgroundColor.ToString(), 
                foregroundText.ToString(), 
                showNameOnSquare150X150Logo.ToStringStartlayout());

            var targetFolder = HelperFolder.CreateSubdirectory(guid);
            
            var nameFile = targetFolder.File("name");
            File.WriteAllText(nameFile.FullName, name);
            
            void CopyImage(BitmapImage vmImage, string defaultImage, TileSize tileSize)
            {
                var source = vmImage?.GetFileStreamSource() ?? new FileInfo(defaultImage);

                source.CopyTo(targetFolder.File(StartmenuSecondaryHelperTile.GetFileNameFromSize(tileSize)).FullName);
            }
            
            CopyImage(visualManifest?.Square70x70Logo, "Assets/Square70x70Logo.png", TileSize.SMALL);

            CopyImage(visualManifest?.Square150x150Logo, "Assets/Square150x150Logo.png", TileSize.MEDIUM);

            CopyImage(null, "Assets/Wide310x150Logo.png", TileSize.WIDE);

            CopyImage(null, "Assets/Square310x310Logo.png", TileSize.LARGE);

            try
            {
                var exitValue = await InvokeHelper(cmdLine);
                if (exitValue != 0)
                {
                    throw new Exception("Could not call Helper, return value: " + exitValue);
                }
            }
            catch (TaskCanceledException)
            {
                targetFolder.Delete(true);
                throw new Exception("Call to Helper timed out.");
            }
            
            var newTile = StartmenuSecondaryHelperTile.New(
                guid, 
                name, 
               (arguments), 
                backgroundColor, 
                foregroundText, 
                showNameOnSquare150X150Logo);
            
            return newTile;
        }

        public static async Task<bool> GetShowNameOnSquare310x310Logo(string tileId)
        {
            ThrowIfHelperMissing();
            
            var cmdLine = HelperAction.ComposeArguments(nameof(IHelperHandler.GetShowNameOnSquare310x310Logo), tileId);
            try
            {
                var exitValue = await InvokeHelper(cmdLine);
                switch (exitValue)
                {
                    case 0:
                        return false;
                    case 1:
                        return true;
                    default:
                        throw new Exception("Could not call Helper, return value: " + exitValue);
                }
            }
            catch (TaskCanceledException)
            {
                throw new Exception("Call to Helper timed out.");
            }
        }

        public static async Task UpdateTile(StartmenuSecondaryHelperTile tile)
        {
            ThrowIfHelperMissing();
            
            var cmdLine = HelperAction.ComposeArguments(nameof(IHelperHandler.UpdateTile), 
                tile.TileID,
                tile.BackgroundColor.ToString(),
                tile.ForegroundTextXml.ToString(),
                tile.ShowNameOnSquare150x150Logo.ToString(),
                tile.ShowNameOnWide310x150Logo.ToString(),
                tile.ShowNameOnSquare310x310Logo.ToString());
            try
            {
                var exitValue = await InvokeHelper(cmdLine);
                if (exitValue != 0)
                {
                    throw new Exception("Could not call Helper, return value: " + exitValue);
                }
            }
            catch (TaskCanceledException)
            {
                throw new Exception("Call to Helper timed out.");
            }
        }

        private static void ThrowIfHelperMissing()
        {
            if (IsHelperInstalled) return;
            throw new Exception("Helper addon ist not installed.");
        }

        private static async Task<int> InvokeHelper(string args, int timeout = 
#if DEBUG
                    30000
#elif !DEBUG
                    3000
#endif
        )
        {
            var taskCompletionSource = new TaskCompletionSource<int>();
            var cancellationToken = new CancellationTokenSource(timeout);
            cancellationToken.Token.Register(() => taskCompletionSource.TrySetCanceled(), false);

            var (hResult, processId) = NativeMethods.Com.ActivateApplication(Aumid, args);

            if (hResult != 0)
            {
                throw Marshal.GetExceptionForHR(hResult);
            }

            using (var process = Process.GetProcessById((int) processId))
            {
                process.EnableRaisingEvents = true;
                var info = process.StartInfo;

                void OnExit(object sender, EventArgs _)
                {
                    if (sender is Process p)
                        try
                        {
                            taskCompletionSource.TrySetResult(p.ExitCode);
                        }
                        catch
                        {
                            taskCompletionSource.TrySetCanceled();
                        }
                    else
                        taskCompletionSource.TrySetCanceled();
                }

                process.Exited += OnExit;
                try
                {
                    return await taskCompletionSource.Task;
                }
                catch (Exception)
                {
#if !DEBUG
                    process.Kill();
#endif
                    throw;
                }
            }
        }

        public static DirectoryInfo GetTileFolder(string tileId)
        {
            return new DirectoryInfo(Path.Combine(HelperFolder.FullName, tileId));
        }
    }
}