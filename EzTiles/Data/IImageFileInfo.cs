using System.Drawing;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EzTiles.Data
{
    public interface IImageFileInfo
    {
        FileInfo File { get; }

        BitmapSource Image { get; }

        string Name { get; }
        
        string Target { get; }
        
        bool IsProxy { get; }
    }
}