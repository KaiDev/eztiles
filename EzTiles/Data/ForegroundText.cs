﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Media;

namespace EzTiles.Data
{
    [TypeConverter(typeof(ForegroundTextConverter))]
    public class ForegroundText
    {
        public static readonly ForegroundText Light = new ForegroundText();
        public static readonly ForegroundText Dark = new ForegroundText();

        public override string ToString()
        {
            if (this == Light)
                return "light";
            if (this == Dark)
                return "dark";
            throw new Exception();
        }

        public static explicit operator Color(ForegroundText self)
        {
            if (self == Light)
                return Colors.White;
            if (self == Dark)
                return Colors.Black;
            throw new Exception();
        }

        public static explicit operator bool(ForegroundText self)
        {
            if (self == Light)
                return true;
            if (self == Dark)
                return false;
            throw new Exception();
        }

        public static implicit operator ForegroundText(string value)
        {
            switch (value)
            {
                case "light": return Light;
                case "dark": return Dark;
                default: throw new Exception();
            }
        }

        public static implicit operator ForegroundText(Color value)
        {
            if (value == Colors.White)
                return Light;
            if (value == Colors.Black)
                return Dark;
            throw new Exception();
        }

        public static implicit operator ForegroundText(bool value)
        {
            return value ? Light : Dark;
        }
    }

    public class ForegroundTextConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(Color))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
            Type destinationType)
        {
            if (destinationType == null)
                throw new ArgumentNullException(nameof(destinationType));

            var foreground = (ForegroundText) value;

            if (destinationType == typeof(bool))
            {
                return (bool) foreground;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}