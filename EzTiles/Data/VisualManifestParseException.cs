﻿using System;
using System.IO;
using System.Windows.Documents;

namespace EzTiles.Data
{
    public class VisualManifestParseException : RichMessageException
    {
        public string VisualManifestFile { get; }

        public VisualManifestParseException(RichMessage message, FileInfo visualManifestFile) :
            this(message, visualManifestFile.FullName)
        {
        }

        public VisualManifestParseException(RichMessage message, string visualManifestFile) :
            base(message)
        {
            VisualManifestFile = visualManifestFile;
        }


        public VisualManifestParseException(RichMessage message, FileInfo visualManifestFile, Exception innerException) :
            this(message, visualManifestFile.FullName, innerException)
        {
        }

        public VisualManifestParseException(RichMessage message, string visualManifestFile, Exception innerException) :
            base(message, innerException)
        {
            VisualManifestFile = visualManifestFile;
        }
    }
}