using System;
using System.Drawing;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using IronStone.Moldinium;
using IWshRuntimeLibrary;

namespace EzTiles.Data
{
    public abstract class UrlFileInfo : IModel, IImageFileInfo
    {
        public static UrlFileInfo FromFile(FileInfo file)
        {
            if (!file.Exists)
            {
                throw new FileNotFoundException($".lnk File {file.FullName} not found", file.FullName);
            }

            if (file.Extension.ToLower() != ".url")
            {
                throw new ArgumentException($"File '{file.FullName}' is not a .url");
            }

            return Models.Create<UrlFileInfo>(info =>
            {
                info.File = file;

                var sc = (IWshURLShortcut) new WshShell().CreateShortcut(info.File.FullName);
                info.Url = sc.TargetPath;
                info.Image = info.File.GetIconBitmapSourceFromPath();
                info.Name = info.File.GetFileNameWithoutExtension();
            });
        }

        public abstract FileInfo File { get; set; }

        public abstract BitmapSource Image { get; set; }

        public abstract string Name { get; set; }

        public string Target => Url;

        public abstract string Url { get; set; }
        
        public bool IsProxy => false;
    }
}