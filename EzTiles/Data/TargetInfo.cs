using System;
using System.IO;
using IronStone.Moldinium;

namespace EzTiles.Data
{
    public class TargetInfoParseException : Exception
    {
        public string TargetInfoFile { get; }

        public TargetInfoParseException(string message, FileInfo targetInfoFile) :
            this(message, targetInfoFile.FullName)
        {
        }

        public TargetInfoParseException(string message, string targetInfoFile) :
            base(message)
        {
            TargetInfoFile = targetInfoFile;
        }


        public TargetInfoParseException(string message, FileInfo targetInfoFile, Exception innerException) :
            this(message, targetInfoFile.FullName, innerException)
        {
        }

        public TargetInfoParseException(string message, string targetInfoFile, Exception innerException) :
            base(message, innerException)
        {
            TargetInfoFile = targetInfoFile;
        }
    }

    public abstract class TargetInfo : IModel
    {
        public abstract string TargetPath { get; set; }

        public abstract FileSystemInfo FileOrFolder { get; set; }

        public abstract VisualManifest VisualManifest { get; set; }


        public static TargetInfo FromFile(string targetPath)
        {
            return Models.Create<TargetInfo>(info => info.Init(targetPath));
        }

        private void Init(string targetPath)
        {
            TargetPath = targetPath;

            var targetFile = new FileInfo(targetPath);
            if (targetFile.Exists)
            {
                FileOrFolder = targetFile;

                if (targetFile.Extension.ToLower() == ".exe" ||
                    targetFile.Extension.ToLower() == ".vbs")
                {
                    var vmFile = GetVisualManifestFile(targetFile);

                    if (vmFile?.Exists == true)
                    {
                        VisualManifest = VisualManifest.FromFile(vmFile);
                    }
                }
            }
            else
            {
                var folder = new DirectoryInfo(targetPath);
                if (folder.Exists) FileOrFolder = folder;
            }
        }

        public static FileInfo GetVisualManifestFile(FileInfo exe)
        {
            var vmPath = Path.Combine(exe.Directory.FullName,
                exe.GetFileNameWithoutExtension() + ".visualelementsmanifest.xml");
            return new FileInfo(vmPath);
        }
    }
}