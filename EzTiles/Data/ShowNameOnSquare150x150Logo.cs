﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace EzTiles.Data
{
    [TypeConverter(typeof(ShowNameOnSquare150x150LogoConverter))]
    public class ShowNameOnSquare150x150Logo
    {
        public static readonly ShowNameOnSquare150x150Logo On = new ShowNameOnSquare150x150Logo();
        public static readonly ShowNameOnSquare150x150Logo Off = new ShowNameOnSquare150x150Logo();

        public string ToStringVisualmanifest()
        {
            if (this == Off)
                return "off";
            if (this == On)
                return "on";
            throw new Exception();
        }
        
        public string ToStringStartlayout()
        {
            if (this == Off)
                return "false";
            if (this == On)
                return "true";
            throw new Exception();
        }

        public static explicit operator bool(ShowNameOnSquare150x150Logo self)
        {
            if (self == Off)
                return false;
            if (self == On)
                return true;
            throw new Exception();
        }

        public static implicit operator ShowNameOnSquare150x150Logo(string value)
        {
            switch (value)
            {
                case "on": return On;
                case "off": return Off;
                case "true": return On;
                case "false": return Off;
                default: throw new Exception();
            }
        }

        public static implicit operator ShowNameOnSquare150x150Logo(bool value)
        {
            return value ? On : Off;
        }
    }

    public class ShowNameOnSquare150x150LogoConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(bool))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value,
            Type destinationType)
        {
            if (destinationType == null)
                throw new ArgumentNullException(nameof(destinationType));

            var foregorund = (ShowNameOnSquare150x150Logo) value;

            if (destinationType == typeof(bool))
            {
                return (bool) foregorund;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}