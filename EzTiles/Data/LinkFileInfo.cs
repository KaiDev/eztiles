﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using EzTiles.Util;
using IronStone.Moldinium;
using IWshRuntimeLibrary;
using Shell32;

namespace EzTiles.Data
{
    public abstract class LinkFileInfo : IModel, IImageFileInfo
    {
        public static LinkFileInfo FromFile(FileInfo lnk)
        {
            if (!lnk.Exists)
            {
                throw new FileNotFoundException($".lnk File '{lnk.FullName}' not found", lnk.FullName);
            }

            if (lnk.Extension.ToLower() != ".lnk")
            {
                throw new ArgumentException($"File '{lnk.FullName}' is not a .lnk");
            }

            return Models.Create<LinkFileInfo>(data => data.Init(lnk));
        }

        //TODO handle lnk to UWP apps
        private void Init(FileInfo lnk)
        {
            File = lnk;
            
            Image = File.GetIconBitmapSourceFromPath();
            
            Name = File.GetFileNameWithoutExtension();

            var target = App.Current.Dispatcher.Invoke(() => MsiShortcutParser.ParseMsiShortcut(File.FullName));

            if (target == null)
            {
                var shellLink = (IWshShortcut) new WshShell().CreateShortcut(lnk.FullName);
                target = shellLink.TargetPath;
                Arguments = shellLink.Arguments;

                WorkingDirectory = shellLink.WorkingDirectory;
            }

            App.Current.Dispatcher.Invoke(() =>
            {
                var shellFolderItem = new Shell().NameSpace(lnk.DirectoryName).ParseName(lnk.Name);
                Name = shellFolderItem.Name;
            });

            if (!string.IsNullOrEmpty(target))
            {
                Target = TargetInfo.FromFile(target);
            }

            if (ProxyManager.PROXY_FOLDER.FullName == File.Directory.Parent.FullName)
            {
                IsProxy = true;
            }
        }

        public abstract FileInfo File { get; set; }

        public abstract TargetInfo Target { get; set; }

        string IImageFileInfo.Target => Target?.TargetPath;

        public abstract string Arguments { get; set; }

        public abstract string WorkingDirectory { get; set; }

        public abstract BitmapSource Image { get; set; }

        public abstract string Name { get; set; }

        public abstract bool IsProxy { get; set; }
    }
}