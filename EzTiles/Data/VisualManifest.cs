﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using EzTiles.Util;
using IronStone.Moldinium;
using static IronStone.Moldinium.Models;
using Color = System.Windows.Media.Color;
using ColorConverter = System.Windows.Media.ColorConverter;

namespace EzTiles.Data
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public abstract class VisualManifest : IModel
    {
        private static readonly Cache<VisualManifest> Cache =
            new Cache<VisualManifest>(file =>
            {
                try
                {
                    return Create<VisualManifest>(temp => temp.InitFromFile(new FileInfo(file)));
                }
                catch (VisualManifestParseException)
                {
                    throw;
                }
                catch (Exception exception)
                {
                    throw new VisualManifestParseException(
                        "Invalid visualelementsmanifest " + RichMessageAction.ForFile(file), file, exception);
                }
            });

        public FileInfo File { get; private set; }

        public abstract Color BackgroundColor { get; set; }
        public abstract ShowNameOnSquare150x150Logo ShowNameOnSquare150x150Logo { get; set; }
        public abstract ForegroundText ForegroundText { get; set; }

        public string Square150x150LogoResourcePath { get; set; }
        public abstract BitmapImage Square150x150Logo { get; set; }

        public string Square70x70LogoResourcePath { get; set; }
        public abstract BitmapImage Square70x70Logo { get; set; }

        public bool PriFileExist { private set; get; }

        [Obsolete("Use 'Models.Create<VisualManifest>()'", true)]
        protected VisualManifest()
        {
        }

        public static VisualManifest New(FileInfo location)
        {
            var vm = Create<VisualManifest>();
            vm.File = location;
            vm.BackgroundColor = SystemParametersExtension.SystemAccentColor; //SystemParameters.WindowGlassColor;
            vm.ForegroundText = ForegroundText.Light;
            vm.ShowNameOnSquare150x150Logo = true;
            vm.PriFileExist = location.Directory.File("resources.pri").Exists;
            vm.Save();
            return Cache.Add(location.FullName, vm);
        }

        public static VisualManifest FromFile(FileInfo file)
        {
            if (!file.Exists) throw new FileNotFoundException();
            var temp = Cache.GetOrAdd(file.FullName);
            temp.InitFromFile(temp.File);
            return temp;
        }

        //follow guidelines
        //https://web.archive.org/web/20150112021208/http://msdn.microsoft.com:80/en-us/library/windows/apps/dn393983.aspx
        private void InitFromFile(FileInfo file)
        {
            File = file;

            var doc = XDocument.Load(file.FullName);

            var layout = doc.Descendants("VisualElements").Single();

            BackgroundColor = (Color) ColorConverter.ConvertFromString(GetAttributeOrThrow(layout, "BackgroundColor"));

            ShowNameOnSquare150x150Logo = GetAttributeOrThrow(layout, "ShowNameOnSquare150x150Logo");

            ForegroundText = GetAttributeOrThrow(layout, "ForegroundText");

            var priFile = file.Directory.File("resources.pri");
            PriFileExist = priFile.Exists;

            var smallPath = (string) layout.Attribute("Square70x70Logo");
            var mediumPath = (string) layout.Attribute("Square150x150Logo");
            if (smallPath != null && mediumPath != null)
            {
                if (!PriFileExist)
                {
                    Square70x70Logo =
                        BitmapExtension.GetBitmapImageInMemory(
                            new FileInfo(file.Directory.FullName + Path.DirectorySeparatorChar + smallPath));
                    Square150x150Logo =
                        BitmapExtension.GetBitmapImageInMemory(
                            new FileInfo(file.Directory.FullName + Path.DirectorySeparatorChar + mediumPath));
                }
                else
                {
                    Square70x70LogoResourcePath = smallPath;
                    Square150x150LogoResourcePath = mediumPath;

                    using (var parser = new PriParser(priFile))
                    {
                        parser.Parse(smallPath, mediumPath, out var smallResolved, out var mediumResolved);
                        Square70x70Logo =
                            BitmapExtension.GetBitmapImageInMemory(
                                new FileInfo(file.DirectoryName + Path.DirectorySeparatorChar + smallResolved));
                        Square150x150Logo =
                            BitmapExtension.GetBitmapImageInMemory(
                                new FileInfo(file.DirectoryName + Path.DirectorySeparatorChar + mediumResolved));
                    }
                }

                return;
            }

            if (smallPath != null || mediumPath != null)
            {
                var message = "VisualManifest file \'" + RichMessageAction.ForFile(File.FullName) +
                              "' must either have both 'Square70x70Logo' and 'Square150x150Logo' attributes or non.";
                throw new VisualManifestParseException(message, File);
            }
        }

        private string GetAttributeOrThrow(XElement element, string name)
        {
            var res = element.Attribute(name);
            if (res != null) return (string) res;
            
            var message = "VisualManifest file '" + RichMessageAction.ForFile(File.FullName) +
                          $"' does not contain required attribute '{name}'.";
            throw new VisualManifestParseException(message, File);

        }

        public Task Save() => Task.Run(() =>
        {
            var xml = new DynamicXDocument
            {
                Root =
                {
                    Application =
                    {
                        VisualElements =
                        {
                            ShowNameOnSquare150x150Logo = ShowNameOnSquare150x150Logo.ToStringVisualmanifest(),
                            ForegroundText = ForegroundText.ToString(),
                            BackgroundColor = BackgroundColor.ToString().Replace("#FF", "#"),
                            Square70x70Logo =
                                Square70x70LogoResourcePath ??
                                Relative(((FileStream) Square70x70Logo?.StreamSource)?.Name, File.Directory),
                            Square150x150Logo = Square150x150LogoResourcePath ??
                                                Relative(((FileStream) Square150x150Logo?.StreamSource)?.Name,
                                                    File.Directory)
                        }
                    }
                }
            }.Build();
            xml.Save(File.FullName);
        });

        private static string Relative(string absolutePath, DirectoryInfo folder)
        {
            return absolutePath == null ? null : new FileInfo(absolutePath).GetPathRelativeToFolder(folder);
        }

        public async Task SaveWithNewImages(FileInfo newFile, string smallLogoPath, string mediumLogoPath)
        {
            var newVm = Create<VisualManifest>();
            newVm.BackgroundColor = BackgroundColor;
            newVm.ForegroundText = ForegroundText;
            newVm.ShowNameOnSquare150x150Logo = ShowNameOnSquare150x150Logo;
            newVm.Square70x70LogoResourcePath = smallLogoPath;
            newVm.Square150x150LogoResourcePath = mediumLogoPath;
            newVm.File = newFile;
            await newVm.Save();
        }
    }
}