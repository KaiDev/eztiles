﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AutoDependencyPropertyMarker;
using EzTiles.AppListData;
using EzTiles.Data;
using IronStone.Moldinium;
using MahApps.Metro.Controls;
using Microsoft.WindowsAPICodePack.Dialogs;
using static EzTiles.Properties.Settings;
using Button = System.Windows.Controls.Button;
using TextBox = System.Windows.Controls.TextBox;

namespace EzTiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            Top = Default.Window_Top;
            Left = Default.Window_Left;
            Height = Default.Window_Height;
            Width = Default.Window_Width;

            if (Default.Window_Maximized)
            {
                WindowState = WindowState.Maximized;
            }

            AppListSource.SortDescriptions.Add(new SortDescription("Title", ListSortDirection.Ascending));
            ProxyListSource.SortDescriptions.Add(new SortDescription("Title", ListSortDirection.Ascending));

            IsStartmenuLoaded = false;
            IsStartmenuChanged = false;
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                Default.Window_Top = RestoreBounds.Top;
                Default.Window_Left = RestoreBounds.Left;
                Default.Window_Height = RestoreBounds.Height;
                Default.Window_Width = RestoreBounds.Width;
                Default.Window_Maximized = true;
            }
            else
            {
                Default.Window_Top = Top;
                Default.Window_Left = Left;
                Default.Window_Height = Height;
                Default.Window_Width = Width;
                Default.Window_Maximized = false;
            }

            Default.Save();
        }

        public new static MainWindow GetWindow(DependencyObject o)
        {
            return (MainWindow) Window.GetWindow(o);
        }

        public static MainWindow Current => App.Current.MainWindow;

        private void ToggleFlyout(object sender, RoutedEventArgs e)
        {
            Flyout.IsOpen = !Flyout.IsOpen;
        }

        private void ChooseFolder(object sender, RoutedEventArgs e)
        {
            var picker = new CommonOpenFileDialog
            {
                IsFolderPicker = true,
                EnsurePathExists = true,
                InitialDirectory = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}",
            };
            if (picker.ShowDialog(this) == CommonFileDialogResult.Ok)
                TextBoxDefaultTileLocation.Text = picker.FileName;
        }

        #region AppList

        public ICollectionView AppListSource { get; } = CollectionViewSource.GetDefaultView(AppListManager.AppList);

        private async void LoadAppList(object sender, RoutedEventArgs e)
        {
            var button = (Button) sender;
            button.IsEnabled = false;

            try
            {
                await AppListManager.LoadAppList();
            }
            catch (Exception ex)
            {
                var content = "An error occured loading the startmenu list.\n" + ex.GetRichMessage();
                new ExceptionDialog(this, ex, "Error", content).ShowDialog();
            }

            button.IsEnabled = true;
        }

        private string _filterStringApps;

        private bool FilterApps(object o)
        {
            var title = ((AppEntry) o).Title;
            return CultureInfo.CurrentCulture.CompareInfo.IndexOf(title, _filterStringApps,
                       CompareOptions.IgnoreCase) >= 0;
        }

        private void AllAppsSearchChanged(object sender, TextChangedEventArgs e)
        {
            var textbox = (TextBox) sender;
            var search = textbox.Text;

            if (string.IsNullOrWhiteSpace(search))
            {
                AppListSource.Filter = null;
            }
            else
            {
                _filterStringApps = search;
                AppListSource.Filter = FilterApps;
            }
        }
        
        #endregion

        #region ProxyList

        public ICollectionView ProxyListSource { get; } = CollectionViewSource.GetDefaultView(ProxyManager.Proxies);

        private async void LoadProxyList(object sender, RoutedEventArgs e)
        {
            var button = (Button) sender;
            button.IsEnabled = false;

            try
            {
                await ProxyManager.LoadProxyList();
            }
            catch (Exception exception)
            {
                var content = "An error occured loading the proxies.\n" + exception.GetRichMessage();
                new ExceptionDialog(this, exception, "Error", content).ShowDialog();
            }

            button.IsEnabled = true;
        }

        private void NewProxy(object sender, RoutedEventArgs e)
        {
            new NewProxy(this).ShowDialog();
        }

        private string _filterStringProxies;

        private bool FilterProxies(object o)
        {
            var title = ((Proxy) o).Title;
            return CultureInfo.CurrentCulture.CompareInfo.IndexOf(title, _filterStringProxies,
                       CompareOptions.IgnoreCase) >= 0;
        }

        private void ProxySearchChanged(object sender, TextChangedEventArgs e)
        {
            var textbox = (TextBox) sender;
            var search = textbox.Text;

            if (string.IsNullOrWhiteSpace(search))
            {
                ProxyListSource.Filter = null;
            }
            else
            {
                _filterStringProxies = search;
                ProxyListSource.Filter = FilterProxies;
            }
        }
        
        #endregion

        #region Startmenu

        public StartmenuManager StartmenuManager { get; } = new StartmenuManager();

        [AutoDependencyPropertyAttribute] public bool IsStartmenuLoaded { get; set; }
        [AutoDependencyPropertyAttribute] public bool IsStartmenuChanged { get; set; }
        
        private async void LoadStartmenu(object sender, RoutedEventArgs e)
        {
            var button = (Button) sender;
            button.IsEnabled = false;
            IsStartmenuLoaded = false;
            try
            {
                await StartmenuManager.LoadStartmenu();
                IsStartmenuLoaded = true;
            }
            catch (Exception exception)
            {
                var content = "An error occured loading the startmenu.\n" + exception.GetRichMessage();
                new ExceptionDialog(this, exception, "Error", content).ShowDialog();
            }

            IsStartmenuChanged = false;
            button.IsEnabled = true;
        }

        private void SaveStartmenu(object sender, RoutedEventArgs _)
        {
            var button = (Button) sender;
            button.IsEnabled = false;

            try
            {
                StartmenuManager.SaveStartmenu();
            }
            catch (Exception exception)
            {
                var message = "An error occured saving the startmenu.\n" + exception.GetRichMessage();
                new ExceptionDialog(this, exception, "Error", message).ShowDialog();
            }

            IsStartmenuChanged = false;
            button.IsEnabled = true;
        }

        public void EditTile(StartmenuDesktopTile tile)
        {
            var linkFileInfo = (LinkFileInfo) tile.Link;
            var vm = linkFileInfo.Target.VisualManifest;
            var editData = Models.Create<VisualManifestEditDataModel>(data =>
            {
                data._link = linkFileInfo;
                
                data.DisplayName = linkFileInfo.Name;
                
                data.ForegroundText = (bool) vm.ForegroundText;
                data.ShowNameOnSquare150x150Logo = (bool) vm.ShowNameOnSquare150x150Logo;
                data.BackgroundColor = vm.BackgroundColor;

                data.Square70x70Logo = vm.Square70x70Logo;
                data.Square70x70LogoFallback = StartmenuDesktopTile.GetTileImageFromIcon(linkFileInfo.Image, TileSize.SMALL);
                data.Square150x150Logo = vm.Square150x150Logo;
                data.Square150x150LogoFallback = StartmenuDesktopTile.GetTileImageFromIcon(linkFileInfo.Image, TileSize.MEDIUM);
                
                data.LockImages = vm.PriFileExist;
            });
            new VisualEditDialog(this, editData).ShowDialog();
        }

        public void EditTile(StartmenuSecondaryHelperTile tile)
        {
            var editData = Models.Create<SecondaryTileEditDataModel>(data =>
                {
                    data.Tile = tile;
                    
                    data.BackgroundColor = tile.BackgroundColor;
                    data.DisplayName = tile.DisplayName;
                    data.ForegroundText = (bool) (ForegroundText) tile.ForegroundText;

                    data.ShowNameOnSquare150x150Logo = tile.ShowNameOnSquare150x150Logo;
                    data.ShowNameOnWide310x150Logo = tile.ShowNameOnWide310x150Logo;
                    data.ShowNameOnSquare310x310Logo = tile.ShowNameOnSquare310x310Logo;

                    data.Square70x70Logo = tile.Square70x70Logo;
                    data.Square150x150Logo = tile.Square150x150Logo;
                    data.Wide310x150Logo = tile.Wide310x150Logo;
                    data.Square310x310Logo = tile.Square310x310Logo;
                });
            new VisualEditDialog(this, editData).ShowDialog();
        }
        
        [AutoDependencyProperty] public bool IsTileInDrag { get; set; }

        private ScrollViewer _groupsListScrollViewer;
        private ScrollViewer GroupsListScrollViewer => _groupsListScrollViewer ??
                                                       (_groupsListScrollViewer =
                                                           GroupsList.FindChildren<ScrollViewer>(true).First());

        public void MoveScrollViewer(double d)
        {
            GroupsListScrollViewer.ScrollToVerticalOffset(GroupsListScrollViewer.VerticalOffset + d);
        }
        
        #endregion
    }
}