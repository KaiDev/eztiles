﻿using System.IO;
using System.Windows.Media;
using EzTiles.Data;
using EzTiles.Util;
using IronStone.Moldinium;

namespace EzTiles.AppListData
{
    public abstract class AppEntry : IModel
    {
        public static AppEntry FromFile(FileInfo lnk)
        {
            var entry = Models.Create<AppEntry>();

            entry.Link = LinkFileInfo.FromFile(lnk);

            return entry;
        }

        public abstract LinkFileInfo Link { get; set; }

        public virtual string Title
            => Link.Name;

        public virtual Color BackgroundColor
            => Link.Target?.VisualManifest?.BackgroundColor ?? SystemParametersExtension.SystemAccentColor;

        public virtual ImageSource Image
            => Link.Image;
    }
}