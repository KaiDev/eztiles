﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using AutoDependencyPropertyMarker;

namespace EzTiles.AppListData
{
    /// <summary>
    /// Interaction logic for Control_AppEntry.xaml
    /// </summary>
    public partial class Control_AppEntry
    {
        [AutoDependencyProperty] public AppEntry Entry { get; set; }

        private StartmenuDesktopTile _tile;
        
        public Control_AppEntry()
        {
            InitializeComponent();
        }

        private MainWindow MainWindow => MainWindow.GetWindow(this);

        private Point _startPoint;
        [AutoDependencyProperty] public bool IsPressed { get; set; }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(null);
            IsPressed = true;
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (IsPressed)
            {
                OnClick();
                IsPressed = false;
            }
        }

        private void OnClick()
        {
//            Debugger.Break();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            IsPressed = false;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsPressed)
            {
                var currentPoint = e.GetPosition(null);
                var distX = currentPoint.X - _startPoint.X;
                var distY = currentPoint.Y - _startPoint.Y;
                var dist = Math.Sqrt(distX * distX + distY * distY);
                if (dist > 10)
                {
                    IsPressed = false;
                    if (MainWindow.StartmenuManager.StartmenuGroups.Count == 0)
                    {
                        MessageBox.Show("Load Startmenu first");
                        return;
                    }

                    if (MainWindow.StartmenuManager.StartmenuGroups.SelectMany(g => g.Tiles).Any(t =>
                        t is StartmenuDesktopTile d && d.Link.File.FullName == Entry.Link.File.FullName))
                    {
                        MessageBox.Show("Tile is already pinned");
                        return;
                    }

                    var main = MainWindow.GetWindow(this);
                    var mouse = PointToScreen(e.GetPosition(this));
                    _tile = StartmenuDesktopTile.New();
                    _tile.Size = TileSize.MEDIUM;
                    _tile.Link = Entry.Link;
                    Control_TileInDrag.Start(main, new Rect(mouse - new Vector(20, 20), new Size(104, 104)), _tile,
                        mouse);
                }
            }
        }
    }
}