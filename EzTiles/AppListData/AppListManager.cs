using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EzTiles.AppListData
{
    public static class AppListManager
    {
        public static ObservableCollection<AppEntry> AppList { get; } = new ObservableCollection<AppEntry>();

        public static async Task LoadAppList() => await Task.Run(() =>
        {
            App.Dispatch(() => AppList.Clear());

            IEnumerable<FileInfo> EnumerateLnks(string folder)
            {
                return new DirectoryInfo(folder).EnumerateFiles("*.lnk", SearchOption.AllDirectories);
            }

            var userStartmenu = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) + "\\Programs";
            var commonStartmenu = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) + "\\Programs";
            foreach (var info in EnumerateLnks(userStartmenu).Concat(EnumerateLnks(commonStartmenu)))
            {
                if (info.Directory.Name == "Startup")
                {
                    continue;
                }
                try
                {
                    var entry = AppEntry.FromFile(info);
                    App.Dispatch(() => AppList.Add(entry));
                }
                catch (Exception ex)
                {
                    throw new RichMessageException(
                        "Could not load app list entry from file " + RichMessageAction.ForFile(info) + ".", ex);
                }
            }
        });
    }
}