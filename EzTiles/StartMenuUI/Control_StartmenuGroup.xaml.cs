﻿using AutoDependencyPropertyMarker;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Castle.Core.Internal;
using EzTiles.Util;

namespace EzTiles
{
    /// <summary>
    /// Interaction logic for StartMenuGroup.xaml
    /// </summary>
    public partial class Control_StartmenuGroup
    {
        public const double CELL_LENGTH = 52;

        [AutoDependencyProperty] public StartmenuGroup Group { get; set; }

        public IEnumerable<Control_TileHolder> TileControls => GridTiles.Children.Cast<Control_TileHolder>();

        public Control_StartmenuGroup(StartmenuGroup group) : this()
        {
            Group = group;
        }

        public Control_StartmenuGroup()
        {
            Loaded += OnInitialized;
            InitializeComponent();
        }

        public void OnInitialized(object sender, EventArgs eventArgs)
        {
            var neededRows = Group.Tiles.Max(tile => tile.Row + tile.Size.GetHeight());

            for (var row = 0; row < neededRows; row++)
            {
                GridTiles.RowDefinitions.Add(new RowDefinition {Height = new GridLength(CELL_LENGTH)});
            }

            var neededColumns = Group.Size.GetSize();

            for (var column = 0; column < neededColumns; column++)
            {
                GridTiles.ColumnDefinitions.Add(new ColumnDefinition {Width = new GridLength(CELL_LENGTH)});
            }

            foreach (var tile in Group.Tiles)
            {
                var holder = new Control_TileHolder(tile);
                GridTiles.Children.Add(holder);
            }

            Group.TilesChanged += (o, args) =>
            {
                if (Group.Tiles.Count == 0)
                {
                    App.Current.MainWindow.StartmenuManager.StartmenuGroups.Remove(Group);
                }

                switch (args.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (var tile in args.NewItems)
                        {
                            var holder = new Control_TileHolder((IStartmenuTileBase) tile);
                            GridTiles.Children.Add(holder);
                            
                            var additionalRows = Group.Tiles.Max(t => t.Row + t.Size.GetHeight()) -
                                GridTiles.RowDefinitions.Count;

                            for (var row = 0; row < additionalRows; row++)
                            {
                                GridTiles.RowDefinitions.Add(new RowDefinition {Height = new GridLength(CELL_LENGTH)});
                            }
                        }

                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (var newItem in args.OldItems)
                        {
                            GridTiles.Children.Remove(TileControls.Single(tile => tile.Tile == newItem));
                        }

                        CleanUpAfterChange();
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        throw new Exception();
                    case NotifyCollectionChangedAction.Move:
                        throw new Exception();
                    case NotifyCollectionChangedAction.Reset:
                        throw new Exception();
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            };
        }

        public void Unpin(Control_TileHolder controlStartmenuTileBase)
        {
            Group.RemoveTile(controlStartmenuTileBase.Tile);
        }

        public void CleanUpAfterChange()
        {
            if (Group.Tiles.Count == 0)
            {
                var main = (MainWindow) Window.GetWindow(this);
                main?.StartmenuManager.StartmenuGroups.Remove(Group);
            }
            else
            {
                RemoveUnnecessaryRows();
            }
        }

        public (bool, List<Cell>) FitsResize(Control_TileHolder tileHolder, TileSize newSize)
        {
            var newCells = GetOccupiedCells(GetCellAt(tileHolder.GetPointMiddleUpperLeft()), newSize);
            var allCells = TileControls.Where(c => !ReferenceEquals(c, tileHolder)).SelectMany(f =>
                GetOccupiedCells(GetCellAt(f.GetPointMiddleUpperLeft()), f.Tile.Size));
            var noIntersects = newCells.Intersect(allCells).IsNullOrEmpty();
            var inBounds = newCells.All(cell => cell.Column < GridTiles.ColumnDefinitions.Count);
            return (noIntersects && inBounds, newCells);
        }

        public void Resize(Control_TileHolder controlStartmenuTileBase, TileSize size)
        {
            if (size == TileSize.SMALL)
            {
                controlStartmenuTileBase.Tile.Size = TileSize.SMALL;
            }
            else
            {
                var (valid, newCells) = FitsResize(controlStartmenuTileBase, size);
                if (valid)
                {
                    controlStartmenuTileBase.Tile.Size = size;
                    var addRows = newCells.Select(c => c.Row).Max() - GridTiles.RowDefinitions.Count + 1;
                    for (var i = 0; i < addRows; i++)
                    {
                        GridTiles.RowDefinitions.Add(new RowDefinition {Height = new GridLength(CELL_LENGTH)});
                    }
                }
            }

            RemoveUnnecessaryRows();
        }

        private void RemoveUnnecessaryRows()
        {
            var rowCount = GridTiles.RowDefinitions.Count;

            var removeList = new List<IEnumerable<Control_TileHolder>>();
            for (var row = 0; row < rowCount; row++)
            {
                if (!Group.Tiles.Any(tile =>
                    tile.Row <= row &&
                    tile.Row + tile.Size.GetHeight() > row))
                {
                    var toDecrease = TileControls.Where(foo => foo.Tile.Row >= row).ToList();
                    removeList.Add(toDecrease);
                }
            }

            foreach (var entry in removeList)
            {
                GridTiles.RowDefinitions.RemoveAt(0);
                foreach (var tile in entry)
                {
                    tile.Tile.Row--;
                }
            }

            _additionalRowCount = 0;
        }

        public void ShowAdditionalyRowsIfNecessary(IStartmenuTileBase tile, Point point)
        {
            int wantedRows;
            var currentNeededRowCount = Group.Tiles.Select(t => t.Row + t.Size.GetHeight()).Max();
            var currentNeededRowCountWithoutCurrentTile =
                Group.Tiles.Where(t => tile != t).Max(t => (int?) t.Row + t.Size.GetHeight()) ?? 0;

            if (!GridTiles.GetRectToScreen().Contains(point) &&
                !GridTiles.GetRectToScreen().Contains(point + new Vector(CELL_LENGTH * tile.Size.GetWidth(), 0)))
            {
                wantedRows = currentNeededRowCount;
                _additionalRowCount = 0;
            }
            else
            {
                point = point + new Vector(0, CELL_LENGTH);
                ;
                wantedRows = Math.Min(
                    Math.Max(
                        GetCellAt(point).Row + tile.Size.GetHeight(),
                        currentNeededRowCount),
                    currentNeededRowCountWithoutCurrentTile + tile.Size.GetHeight());

                _additionalRowCount = wantedRows - currentNeededRowCount;
            }

            GridTiles.RowDefinitions.Clear();
            for (var i = 0; i < wantedRows; i++)
            {
                GridTiles.RowDefinitions.Add(new RowDefinition {Height = new GridLength(CELL_LENGTH)});
            }
        }

        public void HideAdditionalRows(int count)
        {
            GridTiles.RowDefinitions.RemoveRange(0, _additionalRowCount);

            _additionalRowCount = 0;
        }

        private int _additionalRowCount;

        public List<Cell> GetOccupiedCells(Cell cell, TileSize size)
        {
            var cells = new List<Cell>();
            for (var vCell = cell.Row; vCell < size.GetHeight() + cell.Row; vCell++)
            {
                for (var hCell = cell.Column; hCell < size.GetWidth() + cell.Column; hCell++)
                {
                    cells.Add(new Cell(vCell, hCell));
                }
            }

            return cells;
        }

        public Cell GetCellAt(Point p)
        {
            var offset = GridTiles.GetPositionToScreen();

            var row = (int) ((p.Y - offset.Y) / (CELL_LENGTH * VisualTreeHelper.GetDpi(this).DpiScaleX));
            var column = (int) ((p.X - offset.X) / (CELL_LENGTH * VisualTreeHelper.GetDpi(this).DpiScaleY));

            return new Cell(row, column);
        }
    }

    public struct Cell : IEquatable<Cell>
    {
        public readonly int Row;
        public readonly int Column;

        public Cell(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public bool Equals(Cell other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return obj is Cell cell && this == cell;
        }

        public override int GetHashCode()
        {
            return Row.GetHashCode() * 17 ^ Column.GetHashCode();
        }

        public static bool operator ==(Cell x, Cell y)
        {
            return x.Row == y.Row && x.Column == y.Column;
        }

        public static bool operator !=(Cell x, Cell y)
        {
            return !(x == y);
        }
    }
}