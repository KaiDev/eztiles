﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using EzTiles.Util;
using MahApps.Metro.Controls;
using static EzTiles.Control_StartmenuGroup;
using Point = System.Windows.Point;

namespace EzTiles
{
    public class Control_TileInDrag : Window
    {
        public MainWindow MainWindow { get; set; }

        private readonly Control_TileHolder _holder;

        public IStartmenuTileBase Tile { get; set; }

        private bool _isPressed;
        private Point _anchorPoint;

        public static void Start(MainWindow window, Rect rect, IStartmenuTileBase tile, Point mouse, Control_TileHolder origin = null)
        {
            if (window.IsTileInDrag) throw new InvalidOperationException();

            var tileWindow = new Control_TileInDrag(window, tile, origin)
            {
                LeftDpiAware = rect.X + 3,
                TopDpiAware = rect.Y + 3,

                Height = rect.Height - 6,
                Width = rect.Width - 6,

                _isPressed = true,
                _anchorPoint = mouse
            };

            tileWindow.Show();
            tileWindow.CaptureMouse();
        }

        private Control_TileInDrag(MainWindow window, IStartmenuTileBase tile, Control_TileHolder holder)
        {
            Owner = window;
            MainWindow = window;
            Tile = tile;
            _holder = holder;

            Content = new Control_StartmenuTileBase(Tile = tile);

            ShowInTaskbar = false;
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            Topmost = true;
            AllowsTransparency = true;
            Opacity = 0.6;
        }

        private double LeftDpiAware
        {
            set => Left = value / VisualTreeHelper.GetDpi(this).DpiScaleX;
            get => Left * VisualTreeHelper.GetDpi(this).DpiScaleX;
        }

        private double TopDpiAware
        {
            set => Top = value / VisualTreeHelper.GetDpi(this).DpiScaleY;
            get => Top * VisualTreeHelper.GetDpi(this).DpiScaleY;
        }

        protected override void OnLostMouseCapture(MouseEventArgs e)
        {
            if (_isPressed)
            {
                DragEnded();
            }
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            if (_isPressed)
            {
                DragEnded();
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (_isPressed)
            {
                DragEnded();
            }
        }

        private void DragEnded()
        {
            _isPressed = false;
            ReleaseMouseCapture();

            Close();

            _animation?.Stop();

            DragFinished(new Point(LeftDpiAware, TopDpiAware));
        }

        private void DragFinished(Point point)
        {
            MainWindow.IsTileInDrag = false;
            if (_holder != null)
            {
                _holder.Visibility = Visibility.Visible;
            }

            var groupControls = MainWindow.GroupsList.FindChildren<Control_StartmenuGroup>(true);
            var controlStartmenuGroups = groupControls as IList<Control_StartmenuGroup> ?? groupControls.ToList();

            var target = controlStartmenuGroups.SingleOrDefault(control
                => control.GetRectToScreen().Contains(point + HAlf_CELL_DIAGONAL));

            if (target != null)
            {
                var root = point + new Vector(CELL_LENGTH / 2, CELL_LENGTH / 2);
                var rootCell = target.GetCellAt(root);

                var newCells = target.GetOccupiedCells(rootCell, Tile.Size);

                var inGrid = newCells.All(c => c.Column >= 0 &&
                                                 c.Column < target.GridTiles.ColumnDefinitions.Count &&
                                                 c.Row >= 0 &&
                                                 c.Row < target.GridTiles.RowDefinitions.Count);
                if (inGrid)
                {
                    // ReSharper disable once ArrangeThisQualifier
                    var currentCells = target.TileControls.Where(c => c.Tile != this.Tile)
                        .SelectMany(h => target.GetOccupiedCells(new Cell(h.Tile.Row, h.Tile.Column), h.Tile.Size))
                        .ToList();

                    var valid = newCells.All(c => !currentCells.Contains(c));

                    if (valid)
                    {
                        Tile.Column = rootCell.Column;
                        Tile.Row = rootCell.Row;

                        // ReSharper disable once PossibleUnintendedReferenceComparison
                        if (_holder == null)
                        {
                            target.Group.AddTile(Tile);
                            target.CleanUpAfterChange();
                        }
                        else if (target != _holder.GroupControl)
                        {
                            _holder.GroupControl.Group.RemoveTile(Tile);
                            target.Group.AddTile(Tile);

                            target.CleanUpAfterChange();
                        }
                        else
                        {
                            _holder.GroupControl.CleanUpAfterChange();
                        }
                        
                        MainWindow.Current.IsStartmenuChanged = true;
                    }
                }
            }

            foreach (var controlStartmenuGroup in controlStartmenuGroups)
            {
                controlStartmenuGroup.HideAdditionalRows(Tile.Size.GetHeight());
            }
        }

        static readonly Vector HAlf_CELL_DIAGONAL = new Vector(CELL_LENGTH / 2, CELL_LENGTH / 2);

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (_isPressed)
            {
                var currentPoint = PointToScreen(e.GetPosition(this));

                var x = currentPoint.X - _anchorPoint.X;
                var y = currentPoint.Y - _anchorPoint.Y;

                LeftDpiAware += x;
                TopDpiAware += y;
                _anchorPoint = currentPoint;

                HandleScroll();
                HandleExpanding();
            }
        }

        private void HandleExpanding()
        {
            var point = this.GetPositionToScreen();
            foreach (var group in MainWindow.GroupsList.FindChildren<Control_StartmenuGroup>(true))
            {
                group.ShowAdditionalyRowsIfNecessary(Tile, point);
            }
        }

        private DispatcherTimer _animation;

        private double _speed;

        private void HandleScroll()
        {
            var currentPoint = _anchorPoint;

            var listRect = MainWindow.GroupsList.GetRectToScreen();
            if (!listRect.Contains(currentPoint)) return;

            if (listRect.Bottom - currentPoint.Y < 70)
            {
                _speed = (70 - listRect.Bottom + currentPoint.Y) / 70 * 0.3;
                if (_animation == null)
                {
                    StartAnimation(1);
                }
            }
            else if (currentPoint.Y - listRect.Top < 70)
            {
                _speed = (70 - currentPoint.Y + listRect.Top) / 70 * 0.3;
                if (_animation == null)
                {
                    StartAnimation(-1);
                }
            }
            else
            {
                if (_animation != null)
                {
                    _animation.Stop();
                    _animation = null;
                }
            }

            void StartAnimation(int direction)
            {
                _animation = new DispatcherTimer(DispatcherPriority.Normal, Dispatcher)
                {
                    Interval = TimeSpan.FromMilliseconds(10),
                    IsEnabled = true
                };
                var watch = Stopwatch.StartNew();
                _animation.Tick += (sender, args) =>
                {
                    MainWindow.MoveScrollViewer(direction * watch.ElapsedMilliseconds * _speed);
                    watch.Restart();
                };
            }
        }
    }
}