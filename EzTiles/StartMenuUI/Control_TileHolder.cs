﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AutoDependencyPropertyMarker;
using EzTiles.Util;
using static EzTiles.Control_StartmenuGroup;

namespace EzTiles
{
    public class Control_TileHolder : ContentControl
    {
        static Control_TileHolder()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Control_TileHolder),
                new FrameworkPropertyMetadata(typeof(Control_TileHolder)));
        }

        private Grid GridControl => (Grid) Parent;

        public Control_StartmenuGroup GroupControl => (Control_StartmenuGroup) ((StackPanel) GridControl.Parent).Parent;


        public IStartmenuTileBase Tile { get; }

        public Control_TileHolder(IStartmenuTileBase tile)
        {
            Tile = tile;

            ContextMenu = new ContextMenu();

            var unpin = new MenuItem {Header = "Unpin"};
            unpin.Click += UnpinOnClick;
            ContextMenu.Items.Add(unpin);

            if (Tile is SupportsProxy)
            {
                var toProxy = new MenuItem {Header = "Convert to proxy"};
                toProxy.Click += ConvertToProxy;
                ContextMenu.Items.Add(toProxy);
                
                var toSecondary = new MenuItem {Header = "Convert to secondary"};
                toSecondary.Click += ConvertToSecondary;
                ContextMenu.Items.Add(toSecondary);
            }

            var supportedSizes = Tile.GetSupportedSizes();
            // ReSharper disable once PossibleMultipleEnumeration
            if (supportedSizes.Any())
            {
                var resizeOptions = new MenuItem {Header = "Resize"};
                ContextMenu.Items.Add(resizeOptions);
                // ReSharper disable once PossibleMultipleEnumeration
                foreach (var size in supportedSizes)
                {
                    var item = new MenuItem
                    {
                        Header = size.GetName(),
                    };
                    item.Click += (sender, args) => GroupControl.Resize(this, size);
                    resizeOptions.Items.Add(item);
                }
            }

            Content = new Control_StartmenuTileBase(tile);
        }

        private async void ConvertToProxy(object sender, RoutedEventArgs routedEventArgs)
        {
            var tile = (SupportsProxy) Tile;
            await tile.ConvertToProxy();
        }

        private async void ConvertToSecondary(object sender = null, RoutedEventArgs routedEventArgs = null)
        {
            var tile = (SupportsProxy) Tile;
            try
            {
                await tile.ConvertToSecondary();
            }
            catch (Exception e)
            {
                new ExceptionDialog(MainWindow.Current, e, e.Message).ShowDialog();
            }
        }

        private void UnpinOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            GroupControl.Unpin(this);
        }

        private Point _startPoint;

        [AutoDependencyProperty] public bool IsPressed { get; set; }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(null);
            IsPressed = true;
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (IsPressed)
            {
                OnClick();
            }

            IsPressed = false;
        }

        private void OnClick()
        {
            Tile.HandleClick();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            IsPressed = false;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsPressed)
            {
                var currentPoint = e.GetPosition(null);
                var distX = currentPoint.X - _startPoint.X;
                var distY = currentPoint.Y - _startPoint.Y;
                var dist = Math.Sqrt(distX * distX + distY * distY);
                if (dist > 10)
                {
                    Visibility = Visibility.Hidden;
                    IsPressed = false;

                    var main = (MainWindow) Window.GetWindow(this);
                    var rect = new Rect
                    {
                        Location = this.GetPositionToScreen(),
                        Height = ActualHeight,
                        Width = ActualWidth
                    };
                    Control_TileInDrag.Start(main, rect, Tile, PointToScreen(e.GetPosition(this)), this);
                    MainWindow.Current.IsTileInDrag = true;
                }
            }
        }

        static readonly Vector HAlf_CELL_DIAGONAL = new Vector(CELL_LENGTH / 2, CELL_LENGTH / 2);

        public Point GetPointMiddleUpperLeft()
        {
            return this.GetPositionToScreen() + HAlf_CELL_DIAGONAL;
        }
    }
}