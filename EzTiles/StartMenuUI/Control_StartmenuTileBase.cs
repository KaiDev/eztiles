﻿using System.Windows;
using System.Windows.Controls;
using AutoDependencyPropertyMarker;

namespace EzTiles
{
    public class Control_StartmenuTileBase : Control
    {
        static Control_StartmenuTileBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Control_StartmenuTileBase),
                new FrameworkPropertyMetadata(typeof(Control_StartmenuTileBase)));
        }

        public Control_StartmenuTileBase(IStartmenuTileBase tile)
        {
            Tile = tile;
        }

        [AutoDependencyProperty]
        public IStartmenuTileBase Tile
        {
            get; 
            set;
        }
    }
}