using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using EzTiles.Data;
using EzTiles.Properties;
using IWshRuntimeLibrary;

namespace EzTiles
{
    public static class ProxyManager
    {
        public static readonly DirectoryInfo PROXY_FOLDER =
            new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu))
                .CreateSubdirectory(" EzTiles Proxies");

        public static ObservableCollection<Proxy> Proxies { get; } = new ObservableCollection<Proxy>();

        public static async Task LoadProxyList()
        {
            Proxies.Clear();
            await Task.Run(() =>
            {
                PROXY_FOLDER.Create();

                foreach (var folder in PROXY_FOLDER.EnumerateDirectories())
                {
                    try
                    {
                        var file = folder.EnumerateFiles("*.lnk").Single();
                        var proxy = Proxy.FromFile(file);

                        App.Dispatch(() => Proxies.Add(proxy));
                    }
                    catch (Exception exception)
                    {
                        App.Dispatch(() =>
                            new ExceptionDialog(App.Current.MainWindow, 
                                    exception: exception, 
                                    title: "Could not load proxy",
                                    content: "Could not load proxy from folder " + RichMessageAction.ForFile(folder.FullName))
                                .ShowDialog());
                    }
                }
            });
        }

        public static async Task<FileInfo> CreateProxyForDesktop(string name, string args, FileInfo image,
            VisualManifest oldVisualManifest)
        {
            var id = Guid.NewGuid().ToString();
            var folder = PROXY_FOLDER.CreateSubdirectory(id);

            var fileExe = folder.File("proxy.exe");

            using (var provider = CodeDomProvider.CreateProvider("CSharp"))
            {
                string argsEscaped;
                using (var writer = new StringWriter())
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(args), writer, null);
                    argsEscaped = writer.ToString();
                }
                
                var code = $@"
using System;
using System.Reflection;

[assembly: AssemblyTitle({argsEscaped})]" + @"

public class Program
{
    public static void Main()
    {
        " + $@"System.Diagnostics.Process.Start({argsEscaped});" + @"  
    }
}";
                var cr = provider.CompileAssemblyFromSource(new CompilerParameters
                {
                    GenerateExecutable = true,
                    GenerateInMemory = false,
                    TreatWarningsAsErrors = false,
                    WarningLevel = 3,
                    OutputAssembly = fileExe.FullName,
                    CompilerOptions = $"/t:winexe /win32icon:\"{image}\"",
                    ReferencedAssemblies =
                    {
                        "System.dll",
                        "mscorlib.dll"
                    }
                }, code);
    
                if (cr.Errors.HasErrors)
                {
                    throw new Exception("Failed to generate exe\n" + string.Join("\n", cr.Errors.Cast<CompilerError>()));
                }
    
                var lnk = folder.File(name + ".lnk");
    
                if (oldVisualManifest != null)
                {
                    string s = null;
                    string m = null;
                    if (oldVisualManifest.PriFileExist)
                    {
                        s = SavePriImage(lnk.Directory, "SmallImage", oldVisualManifest.Square70x70LogoResourcePath,
                            oldVisualManifest.Square70x70Logo);
                        m = SavePriImage(lnk.Directory, "MediumImage", oldVisualManifest.Square150x150LogoResourcePath,
                            oldVisualManifest.Square150x150Logo);
                    }
                    else if (oldVisualManifest.Square70x70Logo != null)
                    {
                        s = SaveNonPriImage(lnk.Directory, "SmallImage", oldVisualManifest.Square70x70Logo);
                        m = SaveNonPriImage(lnk.Directory, "MediumImage", oldVisualManifest.Square150x150Logo);
                    }
    
                    var newVisualManifestFile = lnk.Directory.File(
                        lnk.Directory.File("proxy.exe").GetFileNameWithoutExtension() + ".visualelementsmanifest.xml");
                    await oldVisualManifest.SaveWithNewImages(newVisualManifestFile, s, m);
                }
    
                var shortcut = (IWshShortcut) new WshShell().CreateShortcut(lnk.FullName);
                shortcut.TargetPath = fileExe.FullName;
                shortcut.Save();
    
                Proxies.Add(Proxy.FromFile(lnk));
    
                return lnk;
            }
        }

        public static FileInfo CreateProxyForUWPTile(string name, string appUserModelId)
        {
            var id = Guid.NewGuid().ToString();
            var folder = PROXY_FOLDER.CreateSubdirectory(id);

            var fileExe = folder.File("proxy.exe");

            using (var provider = CodeDomProvider.CreateProvider("CSharp"))
            {
                string escaped;
                using (var writer = new StringWriter())
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(appUserModelId), writer, null);
                    escaped = writer.ToString();
                }

                var code = $@"
using System;
using System.Runtime.InteropServices;
using System.Reflection;

[assembly: AssemblyTitle({escaped})]" + @"

[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid(""2e941141-7f97-4756-ba1d-9decde894a3d"")]
public interface IApplicationActivationManager {
    int ActivateApplication([MarshalAs(UnmanagedType.LPWStr)] string appUserModelId, [MarshalAs(UnmanagedType.LPWStr)] string arguments, 
        uint options, out uint processId);
}
        
[ComImport, Guid(""45BA127D-10A8-46EA-8AB7-56EA9078943C"")]
public class ApplicationActivationManager { }

public class Program
{
    public static void Main()
    {
        var activation = (IApplicationActivationManager) new ApplicationActivationManager();
        uint unused;
        " + $@"activation.ActivateApplication({escaped}, string.Empty, 0x00000002, out unused);" + @"
    }
}";
                var cr = provider.CompileAssemblyFromSource(new CompilerParameters
                {
                    GenerateExecutable = true,
                    GenerateInMemory = false,
                    TreatWarningsAsErrors = false,
                    WarningLevel = 3,
                    OutputAssembly = fileExe.FullName,
                    CompilerOptions = @"/t:winexe /win32icon:""Assets/proxy_dummy.ico""",
                    ReferencedAssemblies =
                    {
                        "System.dll",
                        "mscorlib.dll"
                    }
                }, code);

                if (cr.Errors.HasErrors)
                {
                    throw new Exception("Failed to generate exe\n" +
                                        string.Join("\n", cr.Errors.Cast<CompilerError>()));
                }

                var lnk = folder.File(name + ".lnk");

                var shortcut = (IWshShortcut) new WshShell().CreateShortcut(lnk.FullName);
                shortcut.TargetPath = fileExe.FullName;
                shortcut.Save();

                Proxies.Add(Proxy.FromFile(lnk));

                return lnk;
            }
        }

        private static string SavePriImage(DirectoryInfo proxyFolder, string fileName, string ogPath, BitmapImage b)
        {
            var savePath = @"Images\" + fileName + Path.GetExtension(ogPath);
            SaveImage(Path.Combine(proxyFolder.FullName, savePath), b);
            return savePath;
        }

        private static string SaveNonPriImage(DirectoryInfo proxyFolder, string filename, BitmapImage b)
        {
            var savePath = @"Images\" + filename + Path.GetExtension(((FileStream) b.StreamSource).Name);
            SaveImage(Path.Combine(proxyFolder.FullName, savePath), b);
            return savePath;
        }

        private static void SaveImage(string s, BitmapImage b)
        {
            var imageFile = new FileInfo(s);
            imageFile.Directory.Create();
            using (var stream = imageFile.Create())
            {
                new PngBitmapEncoder
                {
                    Frames =
                    {
                        BitmapFrame.Create(b)
                    }
                }.Save(stream);
            }
        }
    }
}