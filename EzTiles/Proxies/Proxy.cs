using System.Diagnostics;
using System.IO;
using System.Windows.Media;
using EzTiles.Data;
using EzTiles.Util;
using IronStone.Moldinium;

namespace EzTiles
{
    public abstract class Proxy : IModel
    {
        public static Proxy FromFile(FileInfo lnk)
        {
            var entry = Models.Create<Proxy>();

            entry.Link = LinkFileInfo.FromFile(lnk);

            return entry;
        }

        public abstract LinkFileInfo Link { get; set; }

        public virtual string Title
            => Link.Name;

        public virtual string Description
        {
            get
            {
                var proxyExe = (FileInfo) Link.Target.FileOrFolder;
                var versionInfo = FileVersionInfo.GetVersionInfo(proxyExe.FullName);
                return versionInfo.FileDescription;
            }
        }

        public virtual Color BackgroundColor
            => Link.Target?.VisualManifest?.BackgroundColor ?? SystemParametersExtension.SystemAccentColor;

        public virtual ImageSource Image
            => Link.Image;
    }
}