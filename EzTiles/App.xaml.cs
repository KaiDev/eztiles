﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;

namespace EzTiles
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            QuickConverter.EquationTokenizer.AddNamespace(typeof(object));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Windows.Visibility));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Windows.Media.Colors));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(string));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(EzTiles.Control_StartmenuTileBase));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(EzTiles.Util.BitmapExtension));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Drawing.Icon));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Windows.Interop.Imaging));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Windows.Media.Imaging.BitmapSizeOptions));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(Int32Rect));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(System.Windows.Thickness));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(Environment));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(Debug));
        }

        public new MainWindow MainWindow => (MainWindow) base.MainWindow;

        public new static App Current => (App) Application.Current;

        public static void Dispatch(Action a) => Current.Dispatcher.Invoke(a);

        public static T Dispatch<T>(Func<T> a) => Current.Dispatcher.Invoke(a);
    }
}