﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace EzTiles
{
    public static class BindingExtension
    {
        private static Binding SetSource(this Binding @this, object source)
        {
            if (source is RelativeSourceMode relativeSource)
            {
                @this.RelativeSource = new RelativeSource(relativeSource);
            }
            else
            {
                @this.Source = source;
            }

            return @this;
        }

        public static DependencyProperty BindRelative<T1>(this DependencyProperty property,
            DependencyObject dependencyObject,
            object source, string t1, Func<T1, object> converter = null)
        {
            BindingOperations.SetBinding(dependencyObject, property, NewBinding(t1, source, converter));
            return property;
        }

        public static MultiBinding NewBinding<T1>(string t1, object sourceMode, Func<T1, object> converter = null)
        {
            var binding = new MultiBinding
            {
                Bindings =
                {
                    new Binding(t1).SetSource(sourceMode)
                }
            };
            if (converter != null) binding.Converter = new DynamicMultiConverter<T1>(converter);
            return binding;
        }

        public static DependencyProperty BindRelative<T1, T2>(this DependencyProperty property,
            DependencyObject dependencyObject,
            string t1, string t2, Func<T1, T2, object> converter = null,
            RelativeSourceMode sourceMode = RelativeSourceMode.Self)
        {
            BindingOperations.SetBinding(dependencyObject, property, NewBinding(t1, t2, converter));
            return property;
        }

        public static MultiBinding NewBinding<T1, T2>(
            string t1, string t2, Func<T1, T2, object> converter = null,
            RelativeSourceMode sourceMode = RelativeSourceMode.Self)
        {
            var source = new RelativeSource(sourceMode);
            var binding = new MultiBinding
            {
                Bindings =
                {
                    new Binding(t1) {RelativeSource = source},
                    new Binding(t2) {RelativeSource = source}
                }
            };
            if (converter != null) binding.Converter = new DynamicMultiConverter<T1, T2>(converter);
            return binding;
        }

        public static DependencyProperty BindRelative<T1, T2, T3>(this DependencyProperty property,
            DependencyObject dependencyObject,
            string t1, string t2, string t3, Func<T1, T2, T3, object> converter = null,
            RelativeSourceMode sourceMode = RelativeSourceMode.Self)
        {
            BindingOperations.SetBinding(dependencyObject, property, NewBinding(t1, t2, t3, converter));
            return property;
        }

        public static MultiBinding NewBinding<T1, T2, T3>(
            string t1, string t2, string t3, Func<T1, T2, T3, object> converter = null,
            RelativeSourceMode sourceMode = RelativeSourceMode.Self)
        {
            var source = new RelativeSource(sourceMode);
            var binding = new MultiBinding
            {
                Bindings =
                {
                    new Binding(t1) {RelativeSource = source},
                    new Binding(t2) {RelativeSource = source},
                    new Binding(t3) {RelativeSource = source}
                }
            };
            if (converter != null) binding.Converter = new DynamicMultiConverter<T1, T2, T3>(converter);
            return binding;
        }

        public static DependencyProperty BindRelative<T1, T2, T3, T4>(this DependencyProperty property,
            DependencyObject dependencyObject,
            string t1, string t2, string t3, string t4, Func<T1, T2, T3, T4, object> converter = null,
            RelativeSourceMode sourceMode = RelativeSourceMode.Self)
        {
            BindingOperations.SetBinding(dependencyObject, property, NewBinding(t1, t2, t3, t4, converter));
            return property;
        }

        public static MultiBinding NewBinding<T1, T2, T3, T4>(
            string t1, string t2, string t3, string t4, Func<T1, T2, T3, T4, object> converter = null,
            RelativeSourceMode sourceMode = RelativeSourceMode.Self)
        {
            var source = new RelativeSource(sourceMode);
            var binding = new MultiBinding
            {
                Bindings =
                {
                    new Binding(t1) {RelativeSource = source},
                    new Binding(t2) {RelativeSource = source},
                    new Binding(t3) {RelativeSource = source},
                    new Binding(t4) {RelativeSource = source}
                }
            };
            if (converter != null) binding.Converter = new DynamicMultiConverter<T1, T2, T3, T4>(converter);
            return binding;
        }
    }

    public abstract class DynamicMultiConverter : IMultiValueConverter
    {
        protected static void ThrowGenericNotNullable()
        {
            throw new Exception("Type parameter is not a class or of type Nullable");
        }

        public abstract object Convert(object[] value, Type targetType, object parameter, CultureInfo culture);

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class DynamicMultiConverter<T1> : DynamicMultiConverter
    {
        static DynamicMultiConverter()
        {
            if (default(T1) != null) ThrowGenericNotNullable();
        }

        private readonly Func<T1, object> _action;

        public DynamicMultiConverter(Func<T1, object> action)
        {
            _action = action;
        }

        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return _action(values[0] is T1 ? (T1) values[0] : default(T1));
        }
    }

    public class DynamicMultiConverter<T1, T2> : DynamicMultiConverter
    {
        static DynamicMultiConverter()
        {
            if (default(T1) != null) ThrowGenericNotNullable();
            if (default(T2) != null) ThrowGenericNotNullable();
        }

        private readonly Func<T1, T2, object> _action;

        public DynamicMultiConverter(Func<T1, T2, object> action)
        {
            _action = action;
        }

        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return _action(
                values[0] is T1 ? (T1) values[0] : default(T1),
                values[1] is T2 ? (T2) values[1] : default(T2));
        }
    }

    public class DynamicMultiConverter<T1, T2, T3> : DynamicMultiConverter
    {
        static DynamicMultiConverter()
        {
            if (default(T1) != null) ThrowGenericNotNullable();
            if (default(T2) != null) ThrowGenericNotNullable();
            if (default(T3) != null) ThrowGenericNotNullable();
        }

        private readonly Func<T1, T2, T3, object> _action;

        public DynamicMultiConverter(Func<T1, T2, T3, object> action)
        {
            _action = action;
        }

        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return _action(
                values[0] is T1 ? (T1) values[0] : default(T1),
                values[1] is T2 ? (T2) values[1] : default(T2),
                values[2] is T3 ? (T3) values[2] : default(T3));
        }
    }

    public class DynamicMultiConverter<T1, T2, T3, T4> : DynamicMultiConverter
    {
        static DynamicMultiConverter()
        {
            if (default(T1) != null) ThrowGenericNotNullable();
            if (default(T2) != null) ThrowGenericNotNullable();
            if (default(T3) != null) ThrowGenericNotNullable();
            if (default(T4) != null) ThrowGenericNotNullable();
        }

        private readonly Func<T1, T2, T3, T4, object> _action;

        public DynamicMultiConverter(Func<T1, T2, T3, T4, object> action)
        {
            _action = action;
        }

        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return _action(
                values[0] is T1 ? (T1) values[0] : default(T1),
                values[1] is T2 ? (T2) values[1] : default(T2),
                values[2] is T3 ? (T3) values[2] : default(T3),
                values[3] is T4 ? (T4) values[3] : default(T4));
        }
    }
}