﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using PriFormat;
using SystemColors = System.Windows.SystemColors;

namespace EzTiles.Util
{
    public class PriParser : IDisposable
    {
        private readonly FileStream _priStream;
        private readonly PriFile _priFile;

        public PriParser(FileInfo file)
        {
            _priStream = file.OpenRead();
            _priFile = PriFile.Parse(_priStream);
        }

        public void Parse(string smallPath, string mediumPath, out string smallResolved, out string mediumResolved)
        {
            var priFile = _priFile;
            var primaryResourceMapSection =
                priFile.GetSectionByRef(priFile.PriDescriptorSection.PrimaryResourceMapSection.Value);
            var schemaSection = priFile.GetSectionByRef(primaryResourceMapSection.SchemaSection);

            smallPath = @"\Files\" + smallPath;
            mediumPath = @"\Files\" + mediumPath;

            ResourceMapItem smallReosurceMapItem = null;
            ResourceMapItem mediumResourceMapItem = null;

            foreach (var item in schemaSection.Items)
            {
                if (smallReosurceMapItem == null && item.FullName == smallPath) smallReosurceMapItem = item;
                if (mediumResourceMapItem == null && item.FullName == mediumPath) mediumResourceMapItem = item;
            }

            if (smallReosurceMapItem == null || mediumResourceMapItem == null)
            {
                throw new Exception("atleast one referenced resource is missing in 'resources.pri' file");
            }

            smallResolved = ResolveResourceMapItem(smallReosurceMapItem);
            mediumResolved = ResolveResourceMapItem(mediumResourceMapItem);
        }

        private string ResolveResourceMapItem(ResourceMapItem resourceMapItem)
        {
            var candidate = GetBestCandidate(EnumerateCandidates(resourceMapItem));

            if (candidate.Type == ResourceValueType.AsciiPath ||
                candidate.Type == ResourceValueType.Utf8Path ||
                candidate.Type == ResourceValueType.Path)
            {
                var path = (string) GetData(candidate);

                if (path != null)
                {
                    return path;
                }
            }

            throw new Exception();
        }

        private object GetData(Candidate candidate)
        {
            byte[] data;

            if (candidate.SourceFile == null)
            {
                var byteSpan = candidate.DataItem != null
                    ? _priFile.GetDataItemByRef(candidate.DataItem.Value)
                    : candidate.Data.Value;

                _priStream.Seek(byteSpan.Offset, SeekOrigin.Begin);

                using (var binaryReader = new BinaryReader(_priStream, Encoding.Default, true))
                    data = binaryReader.ReadBytes((int) byteSpan.Length);
            }
            else
            {
                var sourcePath = _priFile.GetReferencedFileByRef(candidate.SourceFile.Value).FullName;

                if (!File.Exists(sourcePath))
                    return null;

                using (var fileStream = File.OpenRead(sourcePath))
                {
                    var sourcePriFile = PriFile.Parse(fileStream);
                    var byteSpan = sourcePriFile.GetDataItemByRef(candidate.DataItem.Value);

                    fileStream.Seek(byteSpan.Offset, SeekOrigin.Begin);

                    using (var binaryReader = new BinaryReader(fileStream, Encoding.Default, true))
                        data = binaryReader.ReadBytes((int) byteSpan.Length);
                }
            }

            switch (candidate.Type)
            {
                case ResourceValueType.AsciiPath:
                case ResourceValueType.AsciiString:
                    return Encoding.ASCII.GetString(data).TrimEnd('\0');
                case ResourceValueType.Utf8Path:
                case ResourceValueType.Utf8String:
                    return Encoding.UTF8.GetString(data).TrimEnd('\0');
                case ResourceValueType.Path:
                case ResourceValueType.String:
                    return Encoding.Unicode.GetString(data).TrimEnd('\0');
                case ResourceValueType.EmbeddedData:
                    return data;
                default:
                    throw new Exception();
            }
        }

        private enum ContrastMode
        {
            STANDARD,
            BLACK,
            WHITE
        }

        private static ContrastMode GetCurrentContrastMode()
        {
            if (!SystemParameters.HighContrast)
            {
                return ContrastMode.STANDARD;
            }

            var windowbrush = SystemColors.WindowBrush;
            if (windowbrush.Color.R == 255 && windowbrush.Color.G == 255 && windowbrush.Color.B == 255)
                return ContrastMode.WHITE;
            else
                return ContrastMode.BLACK;
        }

        private static double GetCurrentDpi()
        {
            var taskbar = NativeMethods.User32.FindWindow("Shell_TrayWnd", null);
            var monitor = NativeMethods.User32.MonitorFromWindow(taskbar, 1);
            NativeMethods.Shcore.GetDpiForMonitor(monitor, NativeMethods.Shcore.DpiType.Effective, out var dpiX,
                out var _);
            return dpiX;
        }

        private Candidate GetBestCandidate(IEnumerable<Candidate> candidates)
        {
            var currentDpi = GetCurrentDpi();
            var currentContrastMode = GetCurrentContrastMode();

            var result = new List<CandidateWithValues>();
            foreach (var candidate in candidates)
            {
                var decisionInfoSection =
                    _priFile.GetSectionByRef(_priFile.PriDescriptorSection.DecisionInfoSections.First());

                var qualifiers = decisionInfoSection.QualifierSets[candidate.QualifierSet].Qualifiers;

                ContrastMode? contrastMode = null;

                void trySetContrastMode(ContrastMode mode)
                {
                    if (contrastMode != null) throw new Exception();
                    contrastMode = mode;
                }

                double? dpi = null;

                void trySetDpi(double newValue)
                {
                    if (dpi != null) throw new Exception();
                    dpi = newValue;
                }

                foreach (var qualifier in qualifiers)
                {
                    switch (qualifier.Type)
                    {
                        case QualifierType.Contrast:
                            if (!Enum.TryParse(qualifier.Value, out ContrastMode parseedMode))
                            {
                                throw new Exception("Unexpected 'contrast' value");
                            }
                            else
                            {
                                trySetContrastMode(parseedMode);
                            }

                            break;
                        case QualifierType.Scale:
                            trySetDpi(double.Parse(qualifier.Value));
                            break;
                    }
                }

                if (dpi == null)
                {
                    throw new Exception("No dpi qualifier found");
                }

                result.Add(new CandidateWithValues(candidate, dpi.Value, contrastMode ?? ContrastMode.STANDARD));
            }

            var filteredContrast = result.Where(can => can.ContrastMode == currentContrastMode).ToList();
            if (!filteredContrast.Any())
                filteredContrast = result.Where(can => can.ContrastMode == ContrastMode.STANDARD).ToList();
            if (!filteredContrast.Any()) throw new Exception("No candidates with applicable contrast found");

            filteredContrast.Sort((can1, can2) => can1.Dpi.CompareTo(can2.Dpi));
            var choosenCandidate =
                filteredContrast.FirstOrDefault(can => can.Dpi > currentDpi) ?? filteredContrast.Last();

            return choosenCandidate.Candidate;
        }

        private class CandidateWithValues
        {
            public ContrastMode ContrastMode { get; }
            public double Dpi { get; }
            public Candidate Candidate { get; }

            public CandidateWithValues(Candidate candidate, double dpi, ContrastMode contrastMode)
            {
                Candidate = candidate;
                ContrastMode = contrastMode;
                Dpi = dpi;
            }
        }

        private IEnumerable<Candidate> EnumerateCandidates(ResourceMapItem resourceMapItem)
        {
            var primaryResourceMapSection =
                _priFile.GetSectionByRef(_priFile.PriDescriptorSection.PrimaryResourceMapSection.Value);

            if (primaryResourceMapSection.CandidateSets.TryGetValue(resourceMapItem.Index, out var candidateSet))
                foreach (var candidate in candidateSet.Candidates)
                    if (candidate != null)
                        yield return candidate;
        }

        public void Dispose()
        {
            _priStream.Dispose();
        }
    }
}