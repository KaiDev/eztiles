﻿using System;
using System.IO;
using System.Windows.Media.Imaging;
using EzTiles.Annotations;

namespace EzTiles.Util
{
    public static class BitmapExtension
    {
        public static FileInfo GetFileStreamSource(this BitmapImage img)
        {
            if (img == null)
            {
                throw new ArgumentNullException();
            }
            return ((FileStream) img.StreamSource).GetFileInfo();
        }

        public static BitmapImage GetBitmapImageInMemory(FileInfo file)
        {
            using (var stream = file.Open(FileMode.Open))
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.StreamSource = stream;
                bitmap.EndInit();
                bitmap.UriSource = file.ToUri();
                bitmap.Freeze();
                return bitmap;
            }
        }

        public static bool IsTransparent(this BitmapImage img)
        {
            if (img == null)
            {
                return true;
            }

            var stride = img.PixelWidth * 4;
            var size = img.PixelHeight * stride;
            var pixels = new byte[size];
            img.CopyPixels(pixels, stride, 0);
            for (int i = 3; i < pixels.Length - 1; i += 4)
            {
                var alpha = pixels[i];
                if (alpha != 255) return true;
            }

            return false;
        }

        public static void SaveAsIcon(this BitmapSource sourceBitmap, FileInfo filePath)
        {
            var fs = new FileStream(filePath.FullName, FileMode.Create);
            // ICO header
            fs.WriteByte(0);
            fs.WriteByte(0);
            fs.WriteByte(1);
            fs.WriteByte(0);
            fs.WriteByte(1);
            fs.WriteByte(0);

            // Image size
            fs.WriteByte((byte) sourceBitmap.Width);
            fs.WriteByte((byte) sourceBitmap.Height);
            // Palette
            fs.WriteByte(0);
            // Reserved
            fs.WriteByte(0);
            // Number of color planes
            fs.WriteByte(0);
            fs.WriteByte(0);
            // Bits per pixel
            fs.WriteByte(32);
            fs.WriteByte(0);

            // Data size, will be written after the data
            fs.WriteByte(0);
            fs.WriteByte(0);
            fs.WriteByte(0);
            fs.WriteByte(0);

            // Offset to image data, fixed at 22
            fs.WriteByte(22);
            fs.WriteByte(0);
            fs.WriteByte(0);
            fs.WriteByte(0);

            // Writing actual data
            var decoder = new PngBitmapEncoder
            {
                Frames =
                {
                    BitmapFrame.Create(sourceBitmap)
                }
            };
            decoder.Save(fs);

            var len = fs.Length - 22;

            fs.Seek(14, SeekOrigin.Begin);
            fs.WriteByte((byte) len);
            fs.WriteByte((byte) (len >> 8));

            fs.Close();
        }
    }
}