﻿using System.Windows;
using System.Windows.Media;

namespace EzTiles.Util
{
    public static class FrameworkElementExtension
    {
        public static Point GetPositionToScreen(this FrameworkElement element) =>
            element.PointToScreen(new Point(0, 0));

        public static Rect GetRectToScreen(this FrameworkElement element) =>
            new Rect(element.GetPositionToScreen(), new Vector(
                element.ActualWidth * VisualTreeHelper.GetDpi(element).DpiScaleX,
                element.ActualHeight * VisualTreeHelper.GetDpi(element).DpiScaleY));
    }
}