﻿using System;
using System.IO;

namespace EzTiles.Util
{
    public static class FileSystemInfoExtension
    {
        public static Uri ToUri(this FileSystemInfo info)
        {
            var str = info.FullName;
            if (info is DirectoryInfo && !str.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                str += Path.DirectorySeparatorChar;
            }

            return new Uri(str);
        }
    }
}