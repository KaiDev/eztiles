﻿using System.Dynamic;
using System.Xml.Linq;

namespace EzTiles.Util
{
    public class DynamicXDocument : DynamicObject
    {
        public XDocument XDocument { get; } = new XDocument();

        public static implicit operator XDocument(DynamicXDocument self) => self.XDocument;

        public dynamic Root => this;

        private XElement _currentElement = null;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (_currentElement == null)
            {
                _currentElement = XDocument.Root;
                if (_currentElement == null)
                {
                    _currentElement = new XElement(binder.Name);
                    XDocument.Add(_currentElement);
                }
            }
            else
            {
                var temp = _currentElement.Element(binder.Name);
                if (temp == null)
                {
                    temp = new XElement(binder.Name);
                    _currentElement.Add(temp);
                }

                _currentElement = temp;
            }

            result = this;
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _currentElement.SetAttributeValue(binder.Name, value);
            _currentElement = null;

            return true;
        }

        public XDocument Build() => XDocument;
    }
}