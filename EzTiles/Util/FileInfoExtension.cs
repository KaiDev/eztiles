﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using EzTiles.Util;

namespace EzTiles
{
    public static class FileInfoExtension
    {
        public static IEnumerable<DirectoryInfo> EnumerateParents(this FileInfo @this)
        {
            var parent = @this.Directory;
            while (parent != null)
            {
                yield return parent;
                parent = parent.Parent;
            }
        }

        public static string GetFileNameWithoutExtension(this FileInfo file)
        {
            return Path.GetFileNameWithoutExtension(file.FullName);
        }

        public static string GetPathRelativeToFolder(this FileInfo file, DirectoryInfo folder)
        {
            var pathUri = file.ToUri();
            var folderUri = folder.ToUri();
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString()
                .Replace('/', Path.DirectorySeparatorChar));
        }

        public static Image GetIconImageFromPath(this FileInfo path)
        {
            using (var icon = path.GetIconFromPath())
            {
                return icon.ToBitmap();
            }
        }

        public static BitmapSource GetIconBitmapSourceFromPath(this FileInfo path)
        {
            using (var icon = path.GetIconFromPath())
            {
                var image = Imaging.CreateBitmapSourceFromHIcon(
                    icon.Handle,
                    new Int32Rect {Width = icon.Width, Height = icon.Height},
                    BitmapSizeOptions.FromEmptyOptions());
                image.Freeze();
                return image;
            }
        }
        
        private static Icon GetIconFromPath(this FileInfo path){
            var fileInfo = new NativeMethods.SHFILEINFO();
            var list = NativeMethods.shell32.SHGetFileInfo(
                path.FullName,
                0,
                ref fileInfo,
                (uint) Marshal.SizeOf(fileInfo),
                0x000004000 /*SHGFI.SysIconIndex*/);
            var iconHandle =
                NativeMethods.Comct132.ImageList_GetIcon(list, fileInfo.iIcon.ToInt32(),
                    0x000000000 /*SHGFI.LargeIcon*/);
            return Icon.FromHandle(iconHandle);

        }

        public static FileInfo File(this DirectoryInfo self, string fileName)
        {
            return new FileInfo(Path.Combine(self.FullName, fileName));
        }
    }
}