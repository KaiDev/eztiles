﻿using System;

namespace EzTiles.Util
{
    public static class Helper
    {
        public static T New<T>(Action<T> init) where T : new()
        {
            var t = new T();
            init(t);
            return t;
        }

        public static T Apply<T>(this T @this, Action<T> action)
        {
            action(@this);
            return @this;
        }

        public static T Cast<T>(this object @this)
        {
            return (T) @this;
        }
    }
}