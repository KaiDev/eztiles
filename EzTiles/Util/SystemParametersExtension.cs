﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Media;

namespace EzTiles.Util
{
    public static class SystemParametersExtension
    {
        private static Color? _systemAccentColor = null;

        public static Color SystemAccentColor
        {
            get
            {
                if (_systemAccentColor == null)
                {
                    var pref = NativeMethods.uxtheme.GetImmersiveUserColorSetPreference(false, false);
                    var count = NativeMethods.uxtheme.GetImmersiveColorSetCount();
                    _systemAccentColor = GetColorFromName(Math.Min(pref, count - 1), "SystemAccent");
                }

                return (Color) _systemAccentColor;
            }
        }


        private static Color GetColorFromName(uint colorSet, string colorName)
        {
            var name = IntPtr.Zero;
            uint colorType;

            try
            {
                name = Marshal.StringToHGlobalUni("Immersive" + colorName);
                colorType = NativeMethods.uxtheme.GetImmersiveColorTypeFromName(name);
                if (colorType == 0xFFFFFFFF) throw new InvalidOperationException();
            }
            finally
            {
                if (name != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(name);
                }
            }

            return GetColorFromType(colorSet, colorType);
        }

        private static Color GetColorFromType(uint colorSet, uint colorType)
        {
            var nativeColor = NativeMethods.uxtheme.GetImmersiveColorFromColorSetEx(colorSet, colorType, false, 0);
            return Color.FromArgb(
                (byte) ((0xFF000000 & nativeColor) >> 24),
                (byte) ((0x000000FF & nativeColor) >> 0),
                (byte) ((0x0000FF00 & nativeColor) >> 8),
                (byte) ((0x00FF0000 & nativeColor) >> 16)
            );
        }
    }
}