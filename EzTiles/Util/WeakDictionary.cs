﻿using System;
using System.Collections.Concurrent;

namespace EzTiles.Util
{
    public class Cache<TValue> where TValue : class
    {
        private readonly Func<string, TValue> _creator;

        private readonly ConcurrentDictionary<string, TValue> _innerDictionary =
            new ConcurrentDictionary<string, TValue>();

        public Cache(Func<string, TValue> creator)
        {
            _creator = creator;
        }

        public TValue Add(string key, TValue vm) => _innerDictionary.GetOrAdd(key, vm);

        public TValue GetOrAdd(string key) => _innerDictionary.GetOrAdd(key, AddValue);

        private TValue AddValue(string s) => _creator(s);
    }
}