﻿using System.IO;

namespace EzTiles
{
    public static class FileStreamExtension
    {
        public static FileInfo GetFileInfo(this FileStream @this)
        {
            return new FileInfo(@this.Name);
        }
    }
}