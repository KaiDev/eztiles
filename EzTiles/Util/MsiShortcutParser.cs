using System.Text;

namespace EzTiles.Util
{
    public static class MsiShortcutParser
    {
        private const int MaxFeatureLength = 38;
        private const int MaxGuidLength = 38;
        private const int MaxPathLength = 1024;

        public static string ParseMsiShortcut(string file)
        {
            var product = new StringBuilder(MaxGuidLength + 1);
            var feature = new StringBuilder(MaxFeatureLength + 1);
            var component = new StringBuilder(MaxGuidLength + 1);

            NativeMethods.msi.MsiGetShortcutTarget(file, product, feature, component);

            var pathLength = MaxPathLength;
            var path = new StringBuilder(pathLength);

            var installState =
                NativeMethods.msi.MsiGetComponentPath(product.ToString(), component.ToString(), path, ref pathLength);

            var result = installState != 3 ? null : path.ToString();
            return result;
        }
    }
}