﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using EzTiles.Util;
using IronStone.Moldinium;

namespace EzTiles
{
    public abstract class StartmenuSecondaryTile : AStartmenuTileBase, IStartmenuTileBase, IModel
    {
        public CanSaveTile CanSaveTile => CanSaveTile.Possible;

        public bool CanCreateProxy(out string whyNot)
        {
            whyNot = "Can not create proxy for secondary tiles";
            return false;
        }

        public string GetProxyArguments()
        {
            throw new NotSupportedException();
        }

        public void NewXml()
        {
            throw new NotSupportedException();
        }

        public void ModifyXml(XElement element)
        {
        }

        public void HandleClick()
        {
            MessageBox.Show("Can not edit a secondary tile");
        }

        public IEnumerable<TileSize> GetSupportedSizes()
        {
            return Enumerable.Empty<TileSize>();
        }

        public const string XML_NAME = "SecondaryTile";

        public static async Task<IStartmenuTileBase> FromXml(XElement tileElement)
        {
            if ((string) tileElement.Attribute("AppUserModelID") == SecondaryManager.Aumid)
            {
                return await StartmenuSecondaryHelperTile.FromXml(tileElement);
            }

            var tile = BaseFromXml<StartmenuSecondaryTile>(tileElement);
            tile.Init(tileElement);
            return tile;
        }

        public void Init(XElement tileElement)
        {
            var color = (string) tileElement.Attribute("BackgroundColor");
            BackgroundColor = color != null
                ? (Color) ColorConverter.ConvertFromString(color)
                : SystemParametersExtension.SystemAccentColor;
            DisplayName = (string) tileElement.Attribute("DisplayName");
        }

        public abstract TileSize Size { get; set; }

        public abstract int Row { get; set; }

        public abstract int Column { get; set; }

        public abstract string DisplayName { get; set; }

        public abstract Color BackgroundColor { get; set; }

        public bool ShowName => true;

        public Color ForegroundText => Colors.White;

        public ImageSource Image => null;

        public static StartmenuSecondaryTile New()
        {
            return Models.Create<StartmenuSecondaryTile>();
        }
    }
}