﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Windows.UI;
using EzTiles.Data;
using EzTiles.Util;
using EzTiles_Common;
using IronStone.Moldinium;
using MahApps.Metro.Controls;
using Color = System.Windows.Media.Color;

namespace EzTiles
{
    public abstract class StartmenuSecondaryHelperTile : AStartmenuTileBase, IStartmenuTileBase, IModel
    {

        public static StartmenuSecondaryHelperTile New(
            string tileId, 
            string displayName, 
            string arguments, 
            Color backgroundColor, 
            ForegroundText foregroundText, 
            ShowNameOnSquare150x150Logo showNameOnSquare150X150Logo)
        {
            var tile = Models.Create<StartmenuSecondaryHelperTile>();
            tile.AppUserModelID = SecondaryManager.Aumid;
            tile.DisplayName = displayName;
            tile.Arguments = arguments;
            tile.TileID = tileId;
            tile.Square70x70Logo = GetImage(tileId, TileSize.SMALL);
            tile.Square150x150Logo = GetImage(tileId, TileSize.MEDIUM);
            tile.Wide310x150Logo = GetImage(tileId, TileSize.WIDE);
            tile.Square310x310Logo = GetImage(tileId, TileSize.LARGE);
            tile.ShowNameOnSquare150x150Logo = (bool) showNameOnSquare150X150Logo;
            tile.ShowNameOnWide310x150Logo = false;
            tile.ShowNameOnWide310x150Logo = false;
            tile.BackgroundColorXml = backgroundColor;
            tile.ForegroundTextXml = foregroundText;
            
            return tile;
        }

        public static async Task<StartmenuSecondaryHelperTile> FromXml(XElement tileElement)
        {
            var tile = BaseFromXml<StartmenuSecondaryHelperTile>(tileElement);
            await tile.Init(tileElement);
            return tile;
        }

        private async Task Init(XElement tileElement)
        {
            AppUserModelID = (string) tileElement.Attribute("AppUserModelID");
            TileID = (string) tileElement.Attribute("TileID");
            Arguments = (string) tileElement.Attribute("Arguments");

            DisplayName = File.ReadAllText(SecondaryManager.GetTileFolder(TileID).File("name").FullName);

            Square70x70Logo = GetImage(TileID, TileSize.SMALL);
            Square150x150Logo = GetImage(TileID, TileSize.MEDIUM);
            Wide310x150Logo = GetImage(TileID, TileSize.WIDE);
            Square310x310Logo = GetImage(TileID, TileSize.LARGE);

            ShowNameOnSquare150x150Logo = (bool) tileElement.Attribute("ShowNameOnSquare150x150Logo");
            ShowNameOnWide310x150Logo = (bool) tileElement.Attribute("ShowNameOnWide310x150Logo");
            ShowNameOnSquare310x310Logo = await SecondaryManager.GetShowNameOnSquare310x310Logo(TileID);

            var color = (string) tileElement.Attribute("BackgroundColor");
            BackgroundColorXml = (Color) ColorConverter.ConvertFromString(color);
            ForegroundTextXml = (string) tileElement.Attribute(nameof(ForegroundText));
        }

        public abstract TileSize Size { get; set; }

        public abstract int Row { get; set; }

        public abstract int Column { get; set; }
        // ReSharper disable once InconsistentNaming
        public string AppUserModelID { get; set; }
        // ReSharper disable once InconsistentNaming
        public string TileID { get; set; }

        public string Arguments { get; set; }

        public abstract string DisplayName { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract BitmapImage Square70x70Logo { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract BitmapImage Square150x150Logo { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract BitmapImage Wide310x150Logo { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract BitmapImage Square310x310Logo { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract bool ShowNameOnSquare150x150Logo { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract bool ShowNameOnWide310x150Logo { get; set; }
        // ReSharper disable once InconsistentNaming
        public abstract bool ShowNameOnSquare310x310Logo { get; set; }

        public abstract Color BackgroundColorXml { get; set; }

        public abstract ForegroundText ForegroundTextXml { get; set; }


        public virtual Color BackgroundColor => BackgroundColorXml.A == 0 ? SystemParametersExtension.SystemAccentColor : BackgroundColorXml;

        public virtual Color ForegroundText => (Color) ForegroundTextXml;

        public virtual ImageSource Image
        {
            get
            {
                switch (Size)
                {
                    case TileSize.SMALL:
                        return Square70x70Logo;
                    case TileSize.MEDIUM:
                        return Square150x150Logo;
                    case TileSize.WIDE:
                        return Wide310x150Logo;
                    case TileSize.LARGE:
                        return Square310x310Logo;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public virtual bool ShowName
        {
            get
            {
                switch (Size)
                {
                    case TileSize.SMALL:
                        return true;
                    case TileSize.MEDIUM:
                        return ShowNameOnSquare150x150Logo;
                    case TileSize.WIDE:
                        return ShowNameOnWide310x150Logo;
                    case TileSize.LARGE:
                        return ShowNameOnSquare310x310Logo;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public CanSaveTile CanSaveTile
        {
            get { return CanSaveTile.MostLikely; }
        }

        public bool CanCreateProxy(out string whyNot)
        {
            whyNot = "Can not create Proxy for a helper Tile";
            return false;
        }

        public void NewXml()
        {
            Source = new XElement(XName.Get(StartmenuSecondaryTile.XML_NAME, "http://schemas.microsoft.com/Start/2014/StartLayout"));
        }

        public void ModifyXml(XElement element)
        {
            element.SetAttributeValue("AppUserModelID", AppUserModelID);
            element.SetAttributeValue("TileID", TileID);
            element.SetAttributeValue("Arguments", Arguments);
            element.SetAttributeValue("DisplayName", DisplayName);
            element.SetAttributeValue("Square150x150LogoUri", GetRelativeString(Square150x150Logo));
            element.SetAttributeValue("Wide310x150LogoUri", GetRelativeString(Wide310x150Logo));
            element.SetAttributeValue("ShowNameOnSquare150x150Logo", ShowNameOnSquare150x150Logo);
            element.SetAttributeValue("ShowNameOnWide310x150Logo", ShowNameOnWide310x150Logo);
            element.SetAttributeValue("BackgroundColor", BackgroundColor);
            element.SetAttributeValue("ForegroundText", ForegroundTextXml);
        }

        public void HandleClick()
        {
            MainWindow.Current.EditTile(this);
        }

        public IEnumerable<TileSize> GetSupportedSizes()
        {
            return Enum.GetValues(typeof(TileSize)).Cast<TileSize>();
        }

        private static BitmapImage GetImage(string tileId, TileSize size)
        {
            var imagesFolder = SecondaryManager.GetTileFolder(tileId);
            var file = imagesFolder.File(GetFileNameFromSize(size));
            var image = BitmapExtension.GetBitmapImageInMemory(file);
            return image;
        }
        
        public static string GetFileNameFromSize(TileSize size)
        {
            switch (size)
            {
                case TileSize.SMALL:
                    return HelperAction.FILE_NAME_SMALL_IMAGE;
                case TileSize.MEDIUM:
                    return HelperAction.FILE_NAME_MEDIUM_IMAGE;
                case TileSize.WIDE:
                    return HelperAction.FILE_NAME_WIDE_IMAGE;
                case TileSize.LARGE:
                    return HelperAction.FILE_NAME_LARGE_IMAGE;
                default:
                    throw new ArgumentOutOfRangeException(nameof(size), size, null);
            }
        }

        private static string GetRelativeString(BitmapImage image)
        {
            return image.GetFileStreamSource().GetPathRelativeToFolder(SecondaryManager.HelperFolder);
        }

        public async Task Save()
        {
            await SecondaryManager.UpdateTile(this);
        }
    }
}