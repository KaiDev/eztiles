﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Xml.Linq;
using IronStone.Moldinium;

namespace EzTiles
{
    public interface IStartmenuTileBase
    {
        bool ShowName { get; }

        string DisplayName { get; }

        Color BackgroundColor { get; }

        Color ForegroundText { get; }

        ImageSource Image { get; }


        StartmenuGroup Group { get; set; }


        TileSize Size { get; set; }

        int Row { get; set; }

        int Column { get; set; }


        CanSaveTile CanSaveTile { get; }

        bool CanCreateProxy(out string whyNot);

        XElement ToXml();

        void NewXml();

        void ModifyXml(XElement element);

        void HandleClick();

        IEnumerable<TileSize> GetSupportedSizes();
    }

    public abstract class AStartmenuTileBase
    {
        internal XElement Source;

        public StartmenuGroup Group { get; set; }

        protected static T BaseFromXml<T>(XElement tileElement) where T : AStartmenuTileBase, IStartmenuTileBase, IModel
        {
            var tile = Models.Create<T>();

            tile.Source = tileElement;
            tile.Row = (int) tileElement.Attribute("Row");
            tile.Column = (int) tileElement.Attribute("Column");
            tile.Size = TileSizeMethods.Parse((string) tileElement.Attribute("Size"));

            return tile;
        }

        public XElement ToXml()
        {
            var tile = (IStartmenuTileBase) this;
            if (Source == null) tile.NewXml();
            Source.SetAttributeValue("Size", tile.Size.ToXml());
            Source.SetAttributeValue("Row", tile.Row);
            Source.SetAttributeValue("Column", tile.Column);
            tile.ModifyXml(Source);
            return Source;
        }
    }
}