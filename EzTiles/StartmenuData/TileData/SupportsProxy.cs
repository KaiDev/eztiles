﻿using System.Threading.Tasks;

namespace EzTiles
{
    public interface SupportsProxy
    {
        Task ConvertToProxy();
        Task ConvertToSecondary();
    }
}