﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources;
using Windows.Management.Deployment;
using Castle.Core.Internal;
using ControlzEx.Standard;
using EzTiles.Data;
using EzTiles.Util;
using IronStone.Moldinium;

namespace EzTiles
{
    public abstract class StartmenuTile : AStartmenuTileBase, IStartmenuTileBase, IModel, SupportsProxy
    {
        public CanSaveTile CanSaveTile => CanSaveTile.Possible;

        public bool CanCreateProxy(out string whyNot)
        {
            whyNot = "Can not create proxy for Windows Store Apps";
            return false;
        }

        public void NewXml()
        {
            throw new NotSupportedException();
        }

        public void ModifyXml(XElement element)
        {
        }

        public void HandleClick()
        {
            MessageBox.Show("Can not edit a primary UWP tile");
        }

        public IEnumerable<TileSize> GetSupportedSizes()
        {
            return Enumerable.Empty<TileSize>();
        }

        public const string XML_NAME = "Tile";

        public static StartmenuTile FromXml(XElement tileElement)
        {
            var tile = BaseFromXml<StartmenuTile>(tileElement);
            tile.Init(tileElement);
            return tile;
        }

        private void Init(XElement tileElement)
        {
            AppUserModelId = (string) tileElement.Attribute("AppUserModelID");

            var split = AppUserModelId.Split('!');

            var packageFamilyName = split[0];
            var packageRelativeAppId = split[1];

            var packages = new PackageManager().FindPackages(packageFamilyName).ToList();
            var package = packages.First();
            var folder = new DirectoryInfo(((dynamic) package).InstalledLocation.Path);
            var manifest = folder.File("AppxManifest.xml");
            try
            {
                using (var text = manifest.OpenText())
                {
                    var doc = XDocument.Load(text);
                    var applications =
                        doc.Descendants("{http://schemas.microsoft.com/appx/manifest/foundation/windows10}Application");
                    var application = applications.Single(
                        element => string.Equals((string) element.Attribute("Id"), packageRelativeAppId,
                            StringComparison.CurrentCultureIgnoreCase));
                    var visualElements = application.Elements()
                        .Single(element => element.Name.LocalName == "VisualElements");

                    var color = (string) visualElements.Attribute("BackgroundColor");
                    BackgroundColor = (Color) ColorConverter.ConvertFromString(color);
                    if (BackgroundColor == Colors.Transparent)
                    {
                        BackgroundColor = SystemParametersExtension.SystemAccentColor;
                    }

                    var displayName = (string) visualElements.Attribute("DisplayName");
                    DisplayName = ResolveDisplayName(displayName, package);
                }
            }
            catch (Exception e)
            {
                new ExceptionDialog(App.Current.MainWindow, e, "Error loading startmenu tile").Show();
            }
        }

        private static string ResolveDisplayName(string displayName, Package package)
        {
            if (displayName.StartsWith("ms-resource"))
            {
                var res = GetName(package.Id.FullName, package.Id.Name, displayName);
                if (res != null) return res;
            }

            return !displayName.IsNullOrEmpty() ? displayName : package.DisplayName;
        }

        //CREDIT https://stackoverflow.com/questions/18219915/get-localized-friendly-names-for-all-winrt-metro-apps-installed-from-wpf-applica
        private static string GetName(string fullName, string packageName, string identifier)
        {
            var sb = new StringBuilder();

            //if name itself contains the package name then assume full url else 
            //format the resource url

            var resourceKey = identifier.ToLower().Contains(packageName.ToLower())
                ? identifier
                : $"ms-resource://{packageName}/resources/{identifier.Split(':')[1].TrimStart('/')}";
            var source = $"@{{{fullName}? {resourceKey}}}";
            var result = NativeMethods.shlwapi.SHLoadIndirectString(source, sb, -1, IntPtr.Zero);

            if (result == 0)
                return sb.ToString();

            //if the above fails then we try the url without /resources/ folder
            //because some apps do not place the resources in that resources folder

            resourceKey = $"ms-resource://{packageName}/{identifier.Split(':')[1].TrimStart('/')}";
            source = $"@{{{fullName}? {resourceKey}}}";
            result = NativeMethods.shlwapi.SHLoadIndirectString(source, sb, -1, IntPtr.Zero);

            return result == 0 ? sb.ToString() : null;
        }

        public abstract TileSize Size { get; set; }

        public abstract int Row { get; set; }

        public abstract int Column { get; set; }


        private string AppUserModelId { get; set; }

        public abstract string DisplayName { get; set; }

        public abstract Color BackgroundColor { get; set; }

        public bool ShowName => true;

        public Color ForegroundText => Colors.White;

        public ImageSource Image => null;

        public async Task ConvertToProxy()
        {
            var lnk = ProxyManager.CreateProxyForUWPTile(DisplayName, AppUserModelId);

            var newTile = StartmenuDesktopTile.New();
            newTile.NewlyCreatedProxy = true;
            newTile.Link = LinkFileInfo.FromFile(lnk);

            Group.Replace(this, newTile, TileSize.MEDIUM);
        }

        public async Task ConvertToSecondary()
        {
            MessageBox.Show("Can not create secondary for an UWP tile.");
        }
    }
}