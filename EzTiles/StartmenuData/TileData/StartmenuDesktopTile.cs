﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using ControlzEx.Standard;
using EzTiles.Data;
using EzTiles.Dialogs;
using EzTiles.Util;
using EzTiles_Common;
using IronStone.Moldinium;
using Color = System.Windows.Media.Color;

namespace EzTiles
{
    public abstract class StartmenuDesktopTile : AStartmenuTileBase, IStartmenuTileBase, IModel, SupportsProxy
    {
        public const string XML_NAME = "DesktopApplicationTile";

        public static StartmenuDesktopTile New()
        {
            return Models.Create<StartmenuDesktopTile>();
        }

        public static StartmenuDesktopTile FromXml(XElement tileElement)
        {
            var tile = BaseFromXml<StartmenuDesktopTile>(tileElement);

            var linkPath = 
                Environment.ExpandEnvironmentVariables((string) tileElement.Attribute("DesktopApplicationLinkPath"));
            var linkFile = new FileInfo(linkPath);

            switch (linkFile.Extension.ToLower())
            {
                case ".lnk":
                    tile.Link = LinkFileInfo.FromFile(linkFile);
                    break;
                case ".url":
                    tile.Link = UrlFileInfo.FromFile(linkFile);
                    break;
                default: throw new Exception($"File {linkFile.FullName} is neither a '.lnk' nor a '.url' file");
            }

            return tile;
        }

        public abstract TileSize Size { get; set; }

        public abstract int Row { get; set; }

        public abstract int Column { get; set; }

        public abstract IImageFileInfo Link { get; set; }


        public VisualManifest VisualManifest => (Link as LinkFileInfo)?.Target?.VisualManifest;


        public virtual bool ShowName => (bool) (VisualManifest?.ShowNameOnSquare150x150Logo ?? true);

        public virtual string DisplayName => Link.Name;

        public virtual Color BackgroundColor => VisualManifest?.BackgroundColor ?? SystemParametersExtension.SystemAccentColor;

        public virtual Color ForegroundText => (Color) (VisualManifest?.ForegroundText ?? Colors.White);

        public virtual ImageSource Image
        {
            get
            {
                switch (Size)
                {
                    case TileSize.SMALL when VisualManifest?.Square70x70Logo != null:
                        return VisualManifest?.Square70x70Logo;
                    case TileSize.MEDIUM when VisualManifest?.Square150x150Logo != null:
                        return VisualManifest?.Square150x150Logo;
                    case TileSize.SMALL:
                    case TileSize.MEDIUM:
                        return GetTileImageFromIcon(Link.Image, Size);
                    default: 
                        throw new InvalidOperationException();
                }
            }
        }

        public static BitmapSource GetTileImageFromIcon(BitmapSource icon, TileSize size)
        {
            var tileLength = size == TileSize.SMALL ? 48 : 100; 
            var iconLength = size == TileSize.SMALL ? 24 : 32;
            var offset = size == TileSize.SMALL ? 0 : -2;
            var drawingVisual = new DrawingVisual();
            using (var dc = drawingVisual.RenderOpen())
            {
                dc.DrawImage(icon, new Rect(tileLength/2 - iconLength/2, tileLength/2 - iconLength/2 + offset, iconLength, iconLength));
            }
            var rtb = new RenderTargetBitmap(tileLength, tileLength, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(drawingVisual);
            return rtb;
        }

        public bool CanCreateProxy(out string whyNot)
        {
            if (Link.IsProxy)
            {
                whyNot = "Can not create a proxy because this tile is already a proxy";
                return false;
            }
            else
            {
                whyNot = "";
                return true;
            }
        }

        public bool NewlyCreatedProxy { get; set; }

        public CanSaveTile CanSaveTile
        {
            get
            {
                switch (Link)
                {
                    case LinkFileInfo l:
                        if (l.Target == null)
                            return CanSaveTile.Unknown;
                        switch (l.Target.FileOrFolder)
                        {
                            case DirectoryInfo _:
                                return CanSaveTile.Impossible;
                            case FileInfo f:
                                switch (f.Extension.ToLower())
                                {
                                    case ".exe" when f.Name.ToLower() == "uninstall.exe":
                                        return CanSaveTile.Impossible;
                                    case ".exe":
                                        return CanSaveTile.Possible;
                                    default:
                                        return CanSaveTile.Impossible;
                                }
                            case null:
                                if (l.Target.TargetPath.ToLower().EndsWith(".exe"))
                                {
                                    if (l.Target.TargetPath.ToLower().EndsWith("uninstall.exe"))
                                    {
                                        return CanSaveTile.Impossible;
                                    }

                                    return CanSaveTile.MostLikely;
                                }
                                else
                                {
                                    return CanSaveTile.Impossible;
                                }
                            default: throw new InvalidOperationException();
                        }
                    case UrlFileInfo _:
                        return CanSaveTile.Impossible;
                    default: throw new InvalidOperationException();
                }
            }
        }

        public void NewXml()
        {
            Source = new XElement(XName.Get(XML_NAME, "http://schemas.microsoft.com/Start/2014/StartLayout"));
        }

        public void ModifyXml(XElement element)
        {
            element.SetAttributeValue("DesktopApplicationLinkPath", Link.File.FullName);
        }

        public void HandleClick()
        {
            if (!(Link is LinkFileInfo lfi))
            {
                MessageBox.Show("Can not edit appearance of desktop tile pointing to a '.url'");
            }
            else
            {
                if (lfi.Target == null)
                {
                    MessageBox.Show("Can not edit appearance of desktop tile pointing to an unknown target");
                }
                else
                {
                    switch (lfi.Target.FileOrFolder)
                    {
                        case FileInfo target:
                            if (lfi.Target.VisualManifest != null)
                            {
                                App.Current.MainWindow.EditTile(this);
                            }
                            else
                            {
                                if (target.Extension.ToLower() != ".exe")
                                {
                                    MessageBox.Show("Cannot create 'visualmanifest' for this tile");
                                }
                                else
                                {
                                    var res = MessageBox.Show(App.Current.MainWindow,
                                        "You need to create a 'visualelementsmanifest' for " +
                                        "this tile to edit its appearance", "Create VisualElementsManifest",
                                        MessageBoxButton.OKCancel);
                                    if (res == MessageBoxResult.Cancel) break;

                                    var newVm = VisualManifest.New(TargetInfo.GetVisualManifestFile(target));
                                    lfi.Target.VisualManifest = newVm;
                                    App.Current.MainWindow.EditTile(this);
                                }
                            }

                            return;
                        case DirectoryInfo _:
                            MessageBox.Show("Can not edit appearance of desktop tile pointing to a folder");
                            return;
                        default:
                            MessageBox.Show(
                                "Can not edit appearance of desktop tile pointing to a missing file/folder");
                            return;
                    }
                }
            }
        }

        public IEnumerable<TileSize> GetSupportedSizes()
        {
            return new List<TileSize>()
            {
                TileSize.SMALL,
                TileSize.MEDIUM
            };
        }

        private string GetProcessArguments()
        {
            if (Link.Target == null)
            {
                return Link.File.FullName;
            }
            return ChooseArgumentsDialog.Show(Link.File.FullName, Link.Target);
        }

        public async Task ConvertToProxy()
        {
            if (Link.IsProxy)
            {
                MessageBox.Show("Can not create proxy because this is already a proxy");
                return;
            }

            var image = new FileInfo(Path.GetTempFileName());
            Link.Image.SaveAsIcon(image);

            var arguments = GetProcessArguments();
            if (arguments == null) return;
            var visualManifest = (Link as LinkFileInfo)?.Target?.VisualManifest;
            var lnk = await ProxyManager.CreateProxyForDesktop(DisplayName, arguments, image, visualManifest);

            var newTile = StartmenuDesktopTile.New();
            newTile.NewlyCreatedProxy = true;
            newTile.Link = LinkFileInfo.FromFile(lnk);

            Group.Replace(this, newTile);
        }
        
        public async Task ConvertToSecondary()
        {
            string arguments;
            // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
            if (Link.IsProxy)
            {
                arguments = Link.Target;
            }
            else
            {
                arguments = GetProcessArguments();
            }
            
            var newTile = await SecondaryManager.CreateNewSecondary(
                DisplayName, 
                arguments, 
                BackgroundColor,
                ForegroundText,
                ShowName,
                VisualManifest);
            Group.Replace(this, newTile);
        }
    }
}