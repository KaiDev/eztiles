﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using EzTiles.Util;
using IronStone.Moldinium;

namespace EzTiles
{
    public abstract class StartmenuFolder : AStartmenuTileBase, IStartmenuTileBase, IModel
    {
        public CanSaveTile CanSaveTile => CanSaveTile.Possible;

        public bool CanCreateProxy(out string whyNot)
        {
            whyNot = "Can not create proxy for folders";
            return false;
        }

        public string GetProxyArguments()
        {
            throw new NotSupportedException();
        }

        public void NewXml()
        {
            throw new NotSupportedException();
        }

        public void ModifyXml(XElement element)
        {
        }

        public void HandleClick()
        {
            //TODO Allow editing of folders
            MessageBox.Show("Editing of folders is not supported right now");
        }

        public IEnumerable<TileSize> GetSupportedSizes()
        {
            return Enumerable.Empty<TileSize>();
        }


        public const string XML_NAME = "Folder";

        public static StartmenuFolder FromXml(XElement tileElement)
        {
            return BaseFromXml<StartmenuFolder>(tileElement);
        }

        public abstract TileSize Size { get; set; }

        public abstract int Row { get; set; }

        public abstract int Column { get; set; }

        public string DisplayName => "Folder (Not Supported)";

        public Color BackgroundColor => SystemParametersExtension.SystemAccentColor;

        public bool ShowName => true;

        public Color ForegroundText => Colors.White;

        public ImageSource Image => null;
    }
}