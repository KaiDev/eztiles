using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Linq;
using EzTiles.Data;
using IronStone.Moldinium;
using Microsoft.Win32;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace EzTiles
{
    public class StartmenuManager : IModel
    {
        public XDocument Document { get; set; }

        public ObservableCollection<StartmenuGroup> StartmenuGroups { get; } =
            new ObservableCollection<StartmenuGroup>();

        public async Task LoadStartmenu()
        {
            var path = Path.GetTempFileName();

            await Task.Run(() =>
            {
                using (var shell = PowerShell.Create())
                {
                    shell
                        .AddCommand("Export-StartLayout")
                        .AddParameter("Path", path)
                        .Invoke();
                }
            });

            StartmenuGroups.Clear();

            await Task.Run(() =>
            {
                using (var text = new FileInfo(path).OpenText())
                {
                    Document = XDocument.Load(text);
                }
            });

            var layout = Document
                .Descendants(@"{http://schemas.microsoft.com/Start/2014/FullDefaultLayout}StartLayout")
                .Single();

            StartmenuSize size;
            switch ((int) layout.Attribute("GroupCellWidth"))
            {
                case 6:
                    size = StartmenuSize.NORMAL;
                    break;
                case 8:
                    size = StartmenuSize.WIDE;
                    break;
                default:
                    throw new Exception();
            }

            foreach (var groupElement in layout.Elements())
            {
                var group = StartmenuGroup.New();

                group.Title = (string) groupElement.Attribute("Name");
                group.Size = size;

                foreach (var tileElement in groupElement.Elements())
                {
                    var tile = await Task.Run(async () =>
                    {
                        switch (tileElement.Name.LocalName)
                        {
                            case StartmenuDesktopTile.XML_NAME:
                                return StartmenuDesktopTile.FromXml(tileElement);
                            case StartmenuTile.XML_NAME:
                                return StartmenuTile.FromXml(tileElement);
                            case StartmenuSecondaryTile.XML_NAME:
                                return await StartmenuSecondaryTile.FromXml(tileElement);
                            case StartmenuFolder.XML_NAME:
                                return StartmenuFolder.FromXml(tileElement);
                            default:
                                throw new Exception();
                        }
                    });
                    group.AddTile(tile);
                }

                StartmenuGroups.Add(group);
            }
        }

        public void SaveStartmenu()
        {
            //TODO Generate fresh Document (necessary for import/export)
            var groupRoot = Document
                .Descendants(XName.Get("StartLayout", "http://schemas.microsoft.com/Start/2014/FullDefaultLayout"))
                .Single();
            groupRoot.RemoveNodes();

            var result = CreateProxyDialog.ShowDialog(Application.Current.MainWindow, StartmenuGroups);
            if (result != true) return;

            groupRoot.Add(StartmenuGroups.Select(group => group.ToXml()));

            var generatedProxies = StartmenuGroups.SelectMany(group => group.Tiles)
                .OfType<StartmenuDesktopTile>()
                .Where(tile => tile.NewlyCreatedProxy)
                .Select(tile => tile.DisplayName)
                .ToList();

            if (generatedProxies.Any())
            {
                var dialog = new WaitingDialog(Application.Current.MainWindow, generatedProxies);
                OpenStartMenu();
                var res = dialog.ShowDialog();
                if (res != true) return;
            }

            foreach (var startmenuDesktopTile in StartmenuGroups.SelectMany(group => @group.Tiles)
                .OfType<StartmenuDesktopTile>())
            {
                startmenuDesktopTile.NewlyCreatedProxy = false;
            }

            Save1709();
        }

        private const int KEYEVENTF_EXTENDEDKEY = 1;
        private const int KEYEVENTF_KEYUP = 2;

        private static void OpenStartMenu()
        {
            NativeMethods.User32.keybd_event((byte) Keys.LWin, 0, KEYEVENTF_EXTENDEDKEY, 0);
            NativeMethods.User32.keybd_event((byte) Keys.LWin, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
        }

        private static string GetWindowTitle(IntPtr winHndl)
        {
            const int nChars = 256;
            var buff = new StringBuilder(nChars);
            var handle = winHndl;

            return NativeMethods.User32.GetWindowText(handle, buff, nChars) > 0 ? buff.ToString() : null;
        }

        private static string GetWindowClassName(IntPtr winHndl)
        {
            const int nChars = 256;
            var buff = new StringBuilder(nChars);
            var handle = winHndl;

            return NativeMethods.User32.GetClassName(handle, buff, nChars) > 0 ? buff.ToString() : null;
        }

        private static void KillExplorer()
        {
            var explorerProcesses = Process.GetProcessesByName("explorer");
            foreach (var explorerProcess in explorerProcesses)
            {
                explorerProcess.Kill();
            }
        }

        private void Save1709()
        {
            var fileuser = new FileInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                @"AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml"));
            if (fileuser.Exists)
            {
                try
                {
                    fileuser.MoveTo(Path.Combine(fileuser.DirectoryName, fileuser.Name + ".bak"));
                }
                catch (IOException)
                {
                }
            }

            const string file = @"C:\Users\Default\AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml";
            using (var writer = XmlWriter.Create(file, new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                Encoding = new UTF8Encoding(false)
            }))
            {
                Document.Save(writer);
            }

            using (var key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Policies\Microsoft\Windows\Explorer", true))
            {
                key?.SetValue("StartLayoutFile", file, RegistryValueKind.String);
                key?.SetValue("LockedStartLayout", 1, RegistryValueKind.DWord);

                KillExplorer();

                const int maxTryCount = 10;
                var count = 0;
                while (count < maxTryCount)
                {
                    OpenStartMenu();

                    var hWnd = NativeMethods.User32.GetForegroundWindow();
                    var title = GetWindowTitle(hWnd);
                    var classname = GetWindowClassName(hWnd);
                    if (title == "Cortana" && classname == "Windows.UI.Core.CoreWindow") break;

                    Thread.Sleep(100);
                    count++;
                }

                if (count == maxTryCount) MessageBox.Show("Please open the Startmenu before you press \"OK\"");

                key?.DeleteValue("StartLayoutFile");
                key?.DeleteValue("LockedStartLayout");

                KillExplorer();
            }

            File.Delete(file);
        }

        private void Save1703()
        {
            var fileuser = new FileInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                @"AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml"));
            if (fileuser.Exists)
            {
                try
                {
                    fileuser.MoveTo(Path.Combine(fileuser.DirectoryName, fileuser.Name + ".bak"));
                }
                catch (IOException)
                {
                }
            }

            const string file = @"C:\Users\Default\AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml";
            using (var writer = XmlWriter.Create(file, new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                Encoding = new UTF8Encoding(false)
            }))
            {
                Document.Save(writer);
            }

            using (var key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Policies\Microsoft\Windows\Explorer", true))
            {
                key?.SetValue("StartLayoutFile", file, RegistryValueKind.String);
                key?.SetValue("LockedStartLayout", 1, RegistryValueKind.DWord);

                RestartService("tiledatamodelsvc");

                MessageBox.Show("Please open the Startmenu before you press \"OK\"");

                key?.DeleteValue("StartLayoutFile");
                key?.DeleteValue("LockedStartLayout");
            }

            RestartService("tiledatamodelsvc");

            MessageBox.Show("Please open the Startmenu before you press \"OK\"");

            File.Delete(file);
        }

        private void Save()
        {
            var file = Path.GetTempFileName();

            using (var writer = XmlWriter.Create(file, new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                Encoding = new UTF8Encoding(false)
            }))
            {
                Document.Save(writer);
            }

            using (var key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Policies\Microsoft\Windows\Explorer", true))
            {
                key.SetValue("StartLayoutFile", file, RegistryValueKind.String);
                key.SetValue("LockedStartLayout", 1, RegistryValueKind.DWord);

                RestartService("tiledatamodelsvc");

                MessageBox.Show("Please open the Startmenu before you press \"OK\"");

                key.DeleteValue("StartLayoutFile");
                key.DeleteValue("LockedStartLayout");
            }

            RestartService("tiledatamodelsvc");

            MessageBox.Show("Please open the Startmenu before you press \"OK\"");

            File.Delete(file);
        }

        private static void RestartService(string serviceName)
        {
            var service = new ServiceController(serviceName);
            service.Stop();
            service.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(5));
            service.Start();
            service.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(5));
        }
    }
}