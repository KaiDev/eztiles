﻿using System;

namespace EzTiles
{
    public enum TileSize
    {
        SMALL = 0,
        MEDIUM = 1,
        WIDE = 2,
        LARGE = 3
    }

    public static class TileSizeMethods
    {
        public static int GetWidth(this TileSize tileSize)
        {
            switch (tileSize)
            {
                case TileSize.SMALL: return 1;
                case TileSize.MEDIUM: return 2;
                case TileSize.WIDE: return 4;
                case TileSize.LARGE: return 4;
                default:
                    throw new ArgumentOutOfRangeException(nameof(tileSize), tileSize, null);
            }
        }

        public static int GetHeight(this TileSize tileSize)
        {
            switch (tileSize)
            {
                case TileSize.SMALL: return 1;
                case TileSize.MEDIUM: return 2;
                case TileSize.WIDE: return 2;
                case TileSize.LARGE: return 4;
                default:
                    throw new ArgumentOutOfRangeException(nameof(tileSize), tileSize, null);
            }
        }

        public static string GetName(this TileSize tileSize)
        {
            switch (tileSize)
            {
                case TileSize.SMALL:
                    return "Small";
                case TileSize.MEDIUM:
                    return "Medium";
                case TileSize.WIDE:
                    return "Wide";
                case TileSize.LARGE:
                    return "Large";
                default:
                    throw new ArgumentOutOfRangeException(nameof(tileSize), tileSize, null);
            }
        }

        public static TileSize Parse(string s)
        {
            switch (s)
            {
                case "1x1": return TileSize.SMALL;
                case "2x2": return TileSize.MEDIUM;
                case "4x2": return TileSize.WIDE;
                case "4x4": return TileSize.LARGE;
            }

            throw new ArgumentException();
        }

        public static string ToXml(this TileSize tileSize)
        {
            return tileSize.GetWidth() + "x" + tileSize.GetHeight();
        }
    }
}