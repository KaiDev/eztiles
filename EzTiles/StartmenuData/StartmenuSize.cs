﻿using System;

namespace EzTiles
{
    public enum StartmenuSize
    {
        WIDE,
        NORMAL
    }
    
    public static class StartmenuSizeMethods
    {
        public static int GetSize(this StartmenuSize size)
        {
            switch (size)
            {
                case StartmenuSize.WIDE:
                    return 8;
                case StartmenuSize.NORMAL:
                    return 6;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}