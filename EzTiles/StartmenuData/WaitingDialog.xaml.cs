﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace EzTiles
{
    public partial class WaitingDialog
    {
        public WaitingDialog(Window window, IEnumerable<string> generatedProxies) : base(window, "Waiting...")
        {
            DataContext = this;
            InitializeComponent();

            ProxiesCheck.Text = string.Join("\n• ", generatedProxies);
        }
    }
}