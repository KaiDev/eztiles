﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Xml.Linq;
using IronStone.Moldinium;

namespace EzTiles
{
    public abstract class StartmenuGroup : IModel, IEnumerable<IStartmenuTileBase>
    {
        public static StartmenuGroup New() => Models.Create<StartmenuGroup>();

        private readonly WatchableList<IStartmenuTileBase> _tiles = new WatchableList<IStartmenuTileBase>();

        public ReadOnlyCollection<IStartmenuTileBase> Tiles { get; }

        public StartmenuGroup()
        {
            Tiles = _tiles.AsReadOnly();
        }

        public void AddTile(IStartmenuTileBase tile)
        {
            var gTile = (AStartmenuTileBase) tile;
            if (gTile.Group != null)
            {
                throw new InvalidOperationException("Tile already has a group");
            }

            gTile.Group = this;
            _tiles.Add(tile);
            
            MainWindow.Current.IsStartmenuChanged = true;
        }

        public void AppendTile(IStartmenuTileBase tile)
        {
            var gTile = (AStartmenuTileBase) tile;
            if (gTile.Group != null)
            {
                throw new InvalidOperationException("Tile already has a group");
            }

            var maxRow = Tiles.Max(t => t.Row + t.Size.GetHeight());

            tile.Row = maxRow;
            tile.Column = 0;

            tile.Group = this;
            _tiles.Add(tile);
            
            MainWindow.Current.IsStartmenuChanged = true;
        }

        public void RemoveTile(IStartmenuTileBase tile)
        {
            _tiles.Remove(tile);
            var gTile = (AStartmenuTileBase) tile;
            gTile.Group = null;
            
            MainWindow.Current.IsStartmenuChanged = true;
        }

        public event NotifyCollectionChangedEventHandler TilesChanged
        {
            add => _tiles.CollectionChanged += value;
            remove => _tiles.CollectionChanged -= value;
        }

        public XElement ToXml()
        {
            var json = new XElement(@"{http://schemas.microsoft.com/Start/2014/StartLayout}Group");
            json.SetAttributeValue("Name", Title);

            foreach (var tile in Tiles)
            {
                json.Add(tile.ToXml());
            }

            return json;
        }

        public abstract StartmenuSize Size { get; set; }

        public abstract string Title { get; set; }

        public IEnumerator<IStartmenuTileBase> GetEnumerator()
        {
            return ((IEnumerable<IStartmenuTileBase>) _tiles).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Replace(IStartmenuTileBase oldTile, IStartmenuTileBase newTile, TileSize maxsize = TileSize.LARGE)
        {
            newTile.Column = oldTile.Column;
            newTile.Row = oldTile.Row;
            newTile.Size = oldTile.Size <= maxsize ? oldTile.Size : maxsize;
            AddTile(newTile);
            RemoveTile(oldTile);
            
            MainWindow.Current.IsStartmenuChanged = true;
        }
    }
}