﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using EzTiles.Data;

namespace EzTiles
{
    /// <summary>
    /// Interaction logic for StartmenuSaveWindow.xaml
    /// </summary>
    public partial class CreateProxyDialog
    {
        public static bool? ShowDialog(Window owner, ObservableCollection<StartmenuGroup> tiles)
        {
            var entries = tiles.SelectMany(group => group.Tiles.Select(tile =>
            {
                var entry = new TileListEntry
                {
                    Tile = tile,
                    CanSaveTile = tile.CanSaveTile,
                    CanCreateProxy = tile.CanCreateProxy(out var whyNot),
                    ToolTipCheckBox = whyNot ?? "Check box if you want to create a proxy tile"
                };
                entry.ShouldGenerateTile = entry.CanCreateProxy &&
                                           entry.CanSaveTile == CanSaveTile.Impossible;
                return entry;
            }));

            var window = new CreateProxyDialog(owner, entries.ToList());
            var showDialog = window.ShowDialog();
            return showDialog;
        }

        public ListCollectionView TilesSource { get; }

        public CreateProxyDialog(Window window, List<TileListEntry> entries) : base(window)
        {
            DataContext = this;
            InitializeComponent();

            TilesSource = new ListCollectionView(entries)
            {
                GroupDescriptions =
                {
                    new PropertyGroupDescription("Tile.Group")
                }
            };
        }

        private async void Save(object _, RoutedEventArgs __)
        {
            try
            {
                foreach (var entry in TilesSource)
                {
                    var e = (TileListEntry) entry;
                    if (e.ShouldGenerateTile)
                    {
                        var tile = (StartmenuDesktopTile) e.Tile;
                        await tile.ConvertToProxy();
                    }
                }
            }
            catch (Exception exception)
            {
                new ExceptionDialog(this, exception,
                        "Error",
                        "An error occured while creating the proxies, please try again")
                    .ShowDialog();
                return;
            }

            Success();
        }
    }

    public class TileListEntry
    {
        public IStartmenuTileBase Tile { get; set; }
        public CanSaveTile CanSaveTile { get; set; }
        public bool CanCreateProxy { get; set; }
        public bool ShouldGenerateTile { get; set; }
        public string ToolTipCheckBox { get; set; }
    }

    // ReSharper disable once UnusedMember.Global
    public static class TileSaveabilityExtension
    {
        // ReSharper disable once UnusedMember.Global used via XAML
        public static string GetSymbol(this CanSaveTile self)
        {
            switch (self)
            {
                case CanSaveTile.Impossible:
                    return '\uE894'.ToString();
                case CanSaveTile.Unknown:
                    return '\uE897'.ToString();
                case CanSaveTile.MostLikely:
                case CanSaveTile.Possible:
                    return '\uE73E'.ToString();
                default: throw new InvalidEnumArgumentException();
            }
        }

        // ReSharper disable once UnusedMember.Global used via XAML
        public static string GetToolTip(this CanSaveTile self)
        {
            switch (self)
            {
                case CanSaveTile.Impossible:
                    return "Won't be able to save this tile";
                case CanSaveTile.Unknown:
                    return "Cannot tell if saving is possible";
                case CanSaveTile.MostLikely:
                    return "Can most likely save this tile";
                case CanSaveTile.Possible:
                    return "Can save this tile";
                default: throw new InvalidEnumArgumentException();
            }
        }
    }

    public enum CanSaveTile
    {
        Impossible,
        Unknown,
        MostLikely,
        Possible
    }
}