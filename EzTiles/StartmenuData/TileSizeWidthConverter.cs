﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace EzTiles
{
    public class TileSizeWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((TileSize) value).GetWidth();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}