﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Castle.Core.Internal;
using EzTiles.Data;
using EzTiles.Util;
using EzTiles_Common;
using Path = System.IO.Path;

namespace EzTiles
{
    /// <summary>
    /// Interaction logic for NewProxy.xaml
    /// </summary>
    public partial class NewProxy
    {
        public NewProxy(Window mainWindow) : base(mainWindow)
        {
            DataContext = this;
            InitializeComponent();
        }

        private async void NewProxy_OnPositiveButtonClick(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;
            try
            {
                if (RadioButtonProxy.IsChecked == true)
                {
                    var icon = new FileInfo(@"Assets\proxy_dummy.ico");

                    var uri = TextBoxArguments.Text;
                    await ProxyManager.CreateProxyForDesktop(TextBoxName.Text, uri, icon, null);
                }
                else if (RadioButtonSecondary.IsChecked == true)
                {
                    var newTile = await SecondaryManager.CreateNewSecondary(
                        TextBoxName.Text,
                        TextBoxArguments.Text,
                        SystemParametersExtension.SystemAccentColor,
                        ForegroundText.Light,
                        true);
                    var groupsList = MainWindow.Current.GroupsList.Items;
                    groupsList[groupsList.Count-1].Cast<StartmenuGroup>().AppendTile(newTile);
                }
                else
                {
                    throw new InvalidOperationException();
                }
                Success();
            }
            catch (Exception exception)
            {
                new StacktraceDialog(this, exception, "Could not create Tile").ShowDialog();
                IsEnabled = true;
            }
        }

        private void ButtonTestArguments_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(TextBoxArguments.Text);
            }
            catch (Exception exception)
            {
                new StacktraceDialog(this, exception, "Could not execute command").ShowDialog();
            }
        }
    }
}