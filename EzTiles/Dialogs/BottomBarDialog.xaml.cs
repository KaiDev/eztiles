using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using AutoDependencyPropertyMarker;
using Castle.Core.Internal;
using static System.Windows.GridLength;
using static EzTiles.Util.Helper;

namespace EzTiles
{
    /// <summary>
    /// Interaktionslogik für BottomBarDialog.xaml
    /// </summary>
    [ContentProperty(nameof(DialogContent))]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class BottomBarDialog : Window
    {
        public BottomBarDialog(Window owner, string title = "") : this()
        {
            Owner = owner;
            if (!title.IsNullOrEmpty())
                Title = title;
        }

        public BottomBarDialog()
        {
            AdditionalButtonLeftEnabled = true;
            AdditionalButtonRightEnabled = true;
            NegativeButtonEnabled = true;
            PositiveButtonEnabled = true;
            ShowInTaskbar = false;

            Content = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition { },
                    new RowDefinition {Height = new GridLength(48)},
                },
                Children =
                {
                    New<ContentPresenter>(
                        presenter => { presenter.SetBinding(ContentProperty, nameof(DialogContent)); }),
                    new Grid
                    {
                        Background = new SolidColorBrush(Color.FromRgb(0xe8, 0xe8, 0xe8)),
                        ColumnDefinitions =
                        {
                            new ColumnDefinition {Width = Auto},
                            new ColumnDefinition {Width = Auto},
                            new ColumnDefinition { },
                            new ColumnDefinition {Width = Auto},
                            new ColumnDefinition {Width = Auto}
                        },
                        Children =
                        {
                            New<Button>(button =>
                            {
                                AdditionalButtonLeft = button;
                                button.Margin = new Thickness(8);
                                button.VerticalContentAlignment = VerticalAlignment.Center;
                                button.MinWidth = 60;
                                Grid.SetColumn(button, 0);
                                button.SetBinding(ContentProperty, nameof(AdditionalButtonLeftContent));
                                button.SetBinding(IsEnabledProperty, nameof(AdditionalButtonLeftEnabled));
                                VisibilityProperty.BindRelative(button, this, nameof(AdditionalButtonLeftContent),
                                    (object text) => text == null ? Visibility.Collapsed : Visibility.Visible);
                            }),
                            New<Button>(button =>
                            {
                                AdditionalButtonRight = button;
                                button.Margin = new Thickness(8);
                                button.VerticalContentAlignment = VerticalAlignment.Center;
                                button.MinWidth = 60;
                                Grid.SetColumn(button, 1);
                                button.SetBinding(ContentProperty, nameof(AdditionalButtonRightContent));
                                button.SetBinding(IsEnabledProperty, nameof(AdditionalButtonRightEnabled));
                                VisibilityProperty.BindRelative(button, this, nameof(AdditionalButtonRightContent),
                                    (object text) => text == null ? Visibility.Collapsed : Visibility.Visible);
                            }),
                            New<Button>(button =>
                            {
                                PositiveButton = button;
                                button.Margin = new Thickness(8);
                                button.VerticalContentAlignment = VerticalAlignment.Center;
                                button.MinWidth = 60;
                                Grid.SetColumn(button, 3);
                                button.SetBinding(ContentProperty, nameof(PositiveButtonContent));
                                button.SetBinding(IsEnabledProperty, nameof(PositiveButtonEnabled));
                                VisibilityProperty.BindRelative(button, this, nameof(PositiveButtonContent),
                                    (object text) => text == null ? Visibility.Collapsed : Visibility.Visible);
                            }),
                            New<Button>(button =>
                            {
                                NegativeButton = button;
                                button.Margin = new Thickness(8);
                                button.VerticalContentAlignment = VerticalAlignment.Center;
                                button.MinWidth = 60;
                                Grid.SetColumn(button, 4);
                                button.SetBinding(ContentProperty, nameof(NegativeButtonContent));
                                button.SetBinding(IsEnabledProperty, nameof(NegativeButtonEnabled));
                                VisibilityProperty.BindRelative(button, this, nameof(NegativeButtonContent),
                                    (object text) => text == null ? Visibility.Collapsed : Visibility.Visible);
                            }),
                        }
                    }.Apply(grid => Grid.SetRow(grid, 1)),
                }
            };
        }

        [AutoDependencyProperty] public FrameworkElement DialogContent { get; set; }
        
        [AutoDependencyProperty] public object AdditionalButtonLeftContent { get; set; }

        [AutoDependencyProperty] public bool AdditionalButtonLeftEnabled { get; set; }

        public Button AdditionalButtonLeft { get; private set; }

        public event RoutedEventHandler AdditionalButtonLeftClick
        {
            add => AdditionalButtonLeft.Click += value;
            remove => AdditionalButtonLeft.Click -= value;
        }

        [AutoDependencyProperty] public object AdditionalButtonRightContent { get; set; }

        [AutoDependencyProperty] public bool AdditionalButtonRightEnabled { get; set; }

        public Button AdditionalButtonRight { get; private set; }

        public event RoutedEventHandler AdditionalButtonRightClick
        {
            add => AdditionalButtonRight.Click += value;
            remove => AdditionalButtonRight.Click -= value;
        }

        [AutoDependencyProperty] public object PositiveButtonContent { get; set; }

        [AutoDependencyProperty] public bool PositiveButtonEnabled { get; set; }

        public Button PositiveButton { get; private set; }

        public event RoutedEventHandler PositiveButtonClick
        {
            add => PositiveButton.Click += value;
            remove => PositiveButton.Click -= value;
        }

        [AutoDependencyProperty] public object NegativeButtonContent { get; set; }

        [AutoDependencyProperty] public bool NegativeButtonEnabled { get; set; }

        public Button NegativeButton { get; private set; }

        public event RoutedEventHandler NegativeButtonClick
        {
            add => NegativeButton.Click += value;
            remove => NegativeButton.Click -= value;
        }

        public void Cancel(object a = null, RoutedEventArgs b = null)
        {
            DialogResult = false;
            Close();
        }

        public void Success(object a = null, RoutedEventArgs b = null)
        {
            DialogResult = true;
            Close();
        }
    }
}