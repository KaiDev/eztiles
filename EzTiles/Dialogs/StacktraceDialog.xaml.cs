﻿using System;
using System.Windows;

namespace EzTiles
{
    public partial class StacktraceDialog
    {
        public Exception Exception { get; }

        public StacktraceDialog(Window owner, Exception exception, string title) : base(owner, title)
        {
            Exception = exception;

            DataContext = this;
            ContentRendered += (a, b) => MaxHeight = Math.Max(ActualHeight, 1200);
            InitializeComponent();
        }
    }
}