﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Navigation;

namespace EzTiles
{
    public class RichMessageException : Exception
    {
        public RichMessage RichMessage { get; }

        public RichMessageException()
        {
        }

        public RichMessageException(RichMessage message) : 
            base(message.ToString())
        {
            RichMessage = message;
        }

        public RichMessageException(RichMessage message, Exception innerException) : 
            base(message.ToString(), innerException)
        {
            RichMessage = message;
        }
    }

    public class RichMessage : IEnumerable
    {
        private readonly List<object> _actions = new List<object>();

        public RichMessage(){}
        
        public RichMessage(string thisMessage)
        {
            Add(thisMessage);
        }

        public static RichMessage operator +(RichMessage r1, RichMessage r2)
        {
            r1.Add(r2);
            return r1;
        }

        public static RichMessage operator +(RichMessage r1, string r2)
        {
            r1.Add(r2);
            return r1;
        }

        public static RichMessage operator +(string s, RichMessage i)
        {
            i.Insert(s, 0);
            return i;
        }

        public Inline ToInline()
        {
            var span = new Span();
            foreach (var action in _actions)
            {
                switch (action)
                {
                    case string str: 
                        span.Inlines.Add(str);
                        break;
                    case RichMessageAction messageAction:
                    {
                        var link = new Hyperlink(new Run(messageAction.Text));
                        link.RequestNavigate += messageAction.Action;
                        span.Inlines.Add(link);
                        break;
                    }
                    case RichMessage richMessage:
                        span.Inlines.Add(richMessage.ToInline());
                        break;
                    default: throw new ArgumentOutOfRangeException();
                }
            }
            return span;
        }
        
        public override string ToString()
        {
            return string.Join("", _actions.Select((obj) =>
            {
                switch (obj)
                {
                    case RichMessageAction richMessageAction:
                        return richMessageAction.Text;
                    case string str:
                        return str;
                    case RichMessage richMessage:
                        return richMessage.ToString();
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }));
        }

        public void Add(string str) => _actions.Add(str);

        public void Add(RichMessageAction messageAction) => _actions.Add(messageAction);
        
        public void Add(RichMessage inline) => _actions.Add(inline);

        public void Insert(string inline, int i) => _actions.Insert(i, inline);

        public IEnumerator GetEnumerator() => _actions.GetEnumerator();
    }

    public class RichMessageAction
    {
        protected internal string Text;
        protected internal RequestNavigateEventHandler Action;

        public static RichMessage operator +(RichMessageAction messageAction, string str) => new RichMessage
        {
            messageAction,
            str,
        };

        public static RichMessage operator +(string str, RichMessageAction messageAction) => new RichMessage
        {
            str,
            messageAction,
        };

        public static RichMessageAction ForFile(FileInfo file) => ForFile(file.FullName);
        
        public static RichMessageAction ForFile(string file)
        {
            return new RichMessageAction
            {
                Text = file,
                Action = (sender, args) => Process.Start("explorer.exe", $"/select, {file}")
            };
        }
    }

    public static class ExceptionExtension {
        public static RichMessage GetRichMessage(this Exception @this)
        {
            return @this is RichMessageException richMessageException
                ? richMessageException.RichMessage
                : new RichMessage(@this.Message);
        } 
    }
}