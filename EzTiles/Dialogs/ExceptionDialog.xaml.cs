﻿using System;
using System.Windows;
using System.Windows.Documents;

namespace EzTiles
{
    /// <summary>
    /// Interaction logic for ExceptionDialog.xaml
    /// </summary>
    public partial class ExceptionDialog
    {
        private readonly Exception _exception;

        private ExceptionDialog(Window owner, Exception exception, string title) : base(owner, title)
        {
            DataContext = this;
            InitializeComponent();
            _exception = exception;
        }

        public ExceptionDialog(Window owner, Exception exception, string title, string content = null) 
            : this(owner, exception, title)
        {
            SetContent(content ?? exception.Message);
        }

        public ExceptionDialog(Window owner, Exception exception, string title, RichMessage content) 
            : this(owner, exception, title)
        {
            SetContent(content.ToInline());
        }

        private void SetContent(Inline content) => TextBlockContent.Inlines.Add(content);

        private void SetContent(string content) => TextBlockContent.Inlines.Add(content);

        private void MoreInfo(object sender, RoutedEventArgs e) =>
            new StacktraceDialog(this, _exception, "Stacktrace").ShowDialog();
    }
}