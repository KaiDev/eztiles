﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AutoDependencyPropertyMarker;

namespace EzTiles.Dialogs
{
    /// <summary>
    /// Interaction logic for ChooseArgumentsDialog.xaml
    /// </summary>
    public partial class ChooseArgumentsDialog
    {
        private readonly string _lnkPath;
        private readonly string _targetPath;
        private string _result;
        
        public static string Show(string lnkPath, string targetPath)
        {
            var dialog = new ChooseArgumentsDialog(lnkPath, targetPath);
            var result = dialog.ShowDialog();
            if (result != true) return null;
            return dialog._result;
        }
        
        public ChooseArgumentsDialog(string lnkPath, string targetPath) : base()
        {
            DataContext = this;
            InitializeComponent();
            
            _lnkPath = lnkPath;
            _targetPath = targetPath;

            RadioButtonLnk.Content = "Call .lnk-File: " + lnkPath;
            RadioButtonTarget.Content = "Call target: " + targetPath;
        }
        
        private void ChooseArgumentsDialog_OnPositiveButtonClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            if (RadioButtonLnk.IsChecked == true)
            {
                _result = _lnkPath;
            }
            else if(RadioButtonTarget.IsChecked == true)
            {
                _result = _targetPath;
            }
            else
            {
                throw new InvalidOperationException();
            }
            Cancel();
        }
    }
}
