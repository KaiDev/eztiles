﻿using System.Runtime.InteropServices;
using static System.Runtime.InteropServices.UnmanagedType;

namespace EzTiles
{
    partial class NativeMethods
    {
        public class Com
        {
            [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
             Guid("2e941141-7f97-4756-ba1d-9decde894a3d")]
            public interface IApplicationActivationManager
            {
                int ActivateApplication([MarshalAs(LPWStr)] string appUserModelId, [MarshalAs(LPWStr)] string arguments,
                    uint options, out uint processId);
            }

            [ComImport, Guid("45BA127D-10A8-46EA-8AB7-56EA9078943C")]
            public class ApplicationActivationManager
            {
            }

            public static (int, uint) ActivateApplication(string appUserModelId, string arguments = "")
            {
                var activation = (IApplicationActivationManager) new ApplicationActivationManager();
                return (activation.ActivateApplication(appUserModelId, arguments, 0x00000002, out var id), id);
            }
        }
    }
}