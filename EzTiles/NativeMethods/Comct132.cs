﻿using System;
using System.Runtime.InteropServices;

namespace EzTiles
{
    static partial class NativeMethods
    {
        public static class Comct132
        {
            [DllImport("Comctl32.dll")]
            public static extern IntPtr ImageList_GetIcon(IntPtr himl, int i, uint flags);
        }
    }
}