﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace EzTiles
{
    static partial class NativeMethods
    {
        // ReSharper disable once InconsistentNaming
        public static class shlwapi
        {
            [DllImport("shlwapi.dll", BestFitMapping = false, CharSet = CharSet.Unicode, ExactSpelling = true,
                SetLastError = false, ThrowOnUnmappableChar = true)]
            public static extern uint SHLoadIndirectString(string pszSource, StringBuilder pszOutBuf, int cchOutBuf,
                IntPtr ppvReserved);
        }
    }
}