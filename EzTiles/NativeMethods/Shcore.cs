﻿using System;
using System.Runtime.InteropServices;

namespace EzTiles
{
    static partial class NativeMethods
    {
        public static class Shcore
        {
            [DllImport("Shcore.dll")]
            public static extern IntPtr GetDpiForMonitor([In] IntPtr hmonitor, [In] DpiType dpiType,
                [Out] out uint dpiX, [Out] out uint dpiY);

            public enum DpiType
            {
                Effective = 0,
                Angular = 1,
                Raw = 2,
            }
        }
    }
}