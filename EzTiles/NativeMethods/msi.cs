﻿using System.Runtime.InteropServices;
using System.Text;
using EzTiles.Util;

namespace EzTiles
{
    internal static partial class NativeMethods
    {
        public static class msi
        {
            [DllImport("msi.dll", CharSet = CharSet.Unicode)]
            public static extern uint MsiGetComponentPath(
                string productCode,
                string componentCode,
                StringBuilder componentPath,
                ref int componentPathBufferSize);

            [DllImport("msi.dll", CharSet = CharSet.Unicode)]
            public static extern uint MsiGetShortcutTarget(
                string targetFile,
                StringBuilder productCode,
                StringBuilder featureID,
                StringBuilder componentCode);
        }
    }
}