﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using AutoDependencyPropertyMarker;

namespace EzTiles
{
    /// <summary>
    /// Interaktionslogik für VisualEditDialog.xaml
    /// </summary>
    public partial class VisualEditDialog : BottomBarDialog
    {
        public static readonly DependencyProperty DataModelProperty = DependencyProperty.Register(
            nameof(DataModel), typeof(EditDataModel), typeof(VisualEditDialog), new PropertyMetadata(default(EditDataModel)));

        public EditDataModel DataModel
        {
            get => (EditDataModel) GetValue(DataModelProperty);
            set => SetValue(DataModelProperty, value);
        }

        private VisualEditDialog(Window window, string title) : base(window, title)
        {
            DataContext = this;
            InitializeComponent();
        }
        
        public VisualEditDialog(Window mainWindow, EditDataModel editData) : this(mainWindow, editData.DisplayName)
        {
            DataModel = editData;

            if (DataModel.LockImages)
            {
                ImagesGrid.IsEnabled = false;
                ResetImageButton.IsEnabled = false;

                foreach (var button in new FrameworkElement[] {ChooseMediumButton, ChooseSmallButton, ImagesGrid})
                {
                    button.ToolTip = "Can't edit images because images are set via resources.pri file";
                    ToolTipService.SetShowOnDisabled(button, true);
                }
            }
        }

        private async void Save(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;

            try
            {
                await DataModel.Save();
            }
            catch (Exception ex)
            {
                new ExceptionDialog(this, ex, "Could not save tile").ShowDialog();
                IsEnabled = true;
                return;
            }
            Success();
        }

        private void ChooseImage(object tile, RoutedEventArgs _)
        {
            DataModel.ChooseImage(this, (TileSize) ((Button)tile).Tag);
        }

        private void ClearImages(object sender, RoutedEventArgs _)
        {
            DataModel.ClearImages();
        }
    }
}