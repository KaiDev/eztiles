﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace EzTiles
{
    public enum ColorComponent
    {
        R,
        G,
        B
    }

    public class ColorComponentConverter : IValueConverter
    {
        public ColorComponent ColorComponent { private get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (Color) value;
            switch (ColorComponent)
            {
                case ColorComponent.R:
                    return (double) color.R;
                case ColorComponent.G:
                    return (double) color.G;
                case ColorComponent.B:
                    return (double) color.B;
                default:
                    throw new ArgumentOutOfRangeException(nameof(parameter), parameter, null);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    //TODO fix input
    public partial class Control_Color
    {
        [AutoDependencyPropertyMarker.AutoDependencyPropertyAttribute]
        public Color Color { get; set; }

        public Control_Color()
        {
            InitializeComponent();
        }

        private void RRangeBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Color = Color.FromArgb(255, (byte) SliderR.Value, Color.G, Color.B);
        }

        private void GRangeBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Color = Color.FromArgb(255, Color.R, (byte) SliderG.Value, Color.B);
        }

        private void BRangeBase_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Color = Color.FromArgb(255, Color.R, Color.G, (byte) SliderB.Value);
        }
    }
}