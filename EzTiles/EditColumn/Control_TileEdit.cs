﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AutoDependencyPropertyMarker;
using EzTiles.Data;

namespace EzTiles
{
    public class Control_TileEdit : Control
    {
        static Control_TileEdit()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Control_TileEdit),
                new FrameworkPropertyMetadata(typeof(Control_TileEdit)));
        }
        
        [AutoDependencyProperty] public string DisplayName { get; set; }
        
        [AutoDependencyProperty] public TileSize ForcedSize { get; set; }
        
        [AutoDependencyProperty] public ImageSource Image { get; set; }
        
        [AutoDependencyProperty] public ImageSource ImageFallback { get; set; }

        [AutoDependencyProperty] public bool ShowName { get; set; }

        [AutoDependencyProperty] public Color BackgroundColor { get; set; }

        [AutoDependencyProperty] public Color ForegroundText { get; set; }
    }
}