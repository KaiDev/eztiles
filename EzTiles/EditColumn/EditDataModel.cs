﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EzTiles
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface EditDataModel
    {
        string DisplayName { get; }
        
        Color BackgroundColor { get; set; }
        bool ForegroundText { get; set; }
        BitmapImage Square70x70Logo { get; set; }
        BitmapSource Square70x70LogoFallback { get; set; }
        BitmapImage Square150x150Logo { get; set; }
        BitmapSource Square150x150LogoFallback { get; set; }
        bool ShowNameOnSquare150x150Logo { get; set; }
        BitmapImage Wide310x150Logo { get; set; }
        bool ShowNameOnWide310x150Logo { get; set; }
        BitmapImage Square310x310Logo { get; set; }
        bool ShowNameOnSquare310x310Logo { get; set; }

        bool IsStateValid { get; }
        bool LockImages { get; }
        bool IsSecondaryTile { get; }
        bool CanClearImages { get; }
        
        void ChooseImage(Window dialog, TileSize forcedSize);
        Task Save();
        void ClearImages();
    }
}