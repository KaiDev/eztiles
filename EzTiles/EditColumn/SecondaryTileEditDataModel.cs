﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using EzTiles.Properties;
using EzTiles.Util;
using IronStone.Moldinium;
using Microsoft.WindowsAPICodePack.Dialogs;
using Microsoft.WindowsAPICodePack.Dialogs.Controls;
using Microsoft.WindowsAPICodePack.Shell;

namespace EzTiles
{
    public abstract class SecondaryTileEditDataModel : EditDataModel, IModel
    {
        
        public StartmenuSecondaryHelperTile Tile { get; set; }
        
        public string DisplayName { get; set; }

        public abstract Color BackgroundColor { get; set; }

        public abstract bool ForegroundText { get; set; }

        public abstract BitmapImage Square70x70Logo { get; set; }
        public abstract BitmapSource Square70x70LogoFallback { get; set; }

        public abstract BitmapImage Square150x150Logo { get; set; }
        public abstract BitmapSource Square150x150LogoFallback { get; set; }
        public abstract bool ShowNameOnSquare150x150Logo { get; set; }

        public abstract BitmapImage Wide310x150Logo { get; set; }
        public abstract bool ShowNameOnWide310x150Logo { get; set; }
        
        public abstract BitmapImage Square310x310Logo { get; set; }
        public abstract bool ShowNameOnSquare310x310Logo { get; set; }
        
        public virtual bool IsStateValid => true;
        public bool LockImages => false;
        public bool IsSecondaryTile => true;
        public bool CanClearImages => false;

        public void ChooseImage(Window dialog, TileSize tileSize)
        {
            BitmapImage oldImage;
            switch (tileSize)
            {
                case TileSize.SMALL:
                    oldImage = Square70x70Logo;
                    break;
                case TileSize.MEDIUM:
                    oldImage = Square150x150Logo;
                    break;
                case TileSize.WIDE:
                    oldImage = Wide310x150Logo;
                    break;
                case TileSize.LARGE:
                    oldImage = Square310x310Logo;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(tileSize), tileSize, null);
            }
            
            var (file, selection) = OpenImageChooser(dialog, DisplayName, GenArgs(tileSize), oldImage);
            if (file == null) return;

            if (selection[(int) TileSize.SMALL])
            {
                var image = BitmapExtension.GetBitmapImageInMemory(file);
                Square70x70Logo = image;
            }

            if (selection[(int) TileSize.MEDIUM])
            {
                var image = BitmapExtension.GetBitmapImageInMemory(file);
                Square150x150Logo = image;
            }

            if (selection[(int) TileSize.WIDE])
            {
                var image = BitmapExtension.GetBitmapImageInMemory(file);
                Wide310x150Logo = image;
            }

            if (selection[(int) TileSize.LARGE])
            {
                var image = BitmapExtension.GetBitmapImageInMemory(file);
                Square310x310Logo = image;
            }
        }
        
        private static bool[] GenArgs(params TileSize[] sizes)
        {
            var arr = new bool[4];
            foreach (var size in sizes)
            {
                arr[(int) size] = true;
            }

            return arr;
        }

        private (FileInfo, bool[]) OpenImageChooser(Window window, string title, bool[] defaultSizes, BitmapImage oldImage)
        {
            var initialDirectory = Directory.Exists(Settings.Default.Images_Location)
                ? Settings.Default.Images_Location
                : @"c:\";

            var checkboxSmall = new CommonFileDialogCheckBox("Small (1x1)");
            var checkboxMedium = new CommonFileDialogCheckBox("Medium (2x2)");
            var checkboxWide = new CommonFileDialogCheckBox("Wide (4x2)");
            var checkboxLarge = new CommonFileDialogCheckBox("Large (4x4)");

            var dialog = new CommonOpenFileDialog
            {
                Title = title,
                InitialDirectory = initialDirectory,
                Filters = {new CommonFileDialogFilter("Image files", "*.jpeg;*.jpg;*.png")},
                EnsureFileExists = true,
                Controls =
                {
                    checkboxMedium,
                    checkboxSmall,
                    checkboxWide,
                    checkboxLarge
                }
            };

            checkboxSmall.IsChecked = defaultSizes[(int) TileSize.SMALL];
            checkboxMedium.IsChecked = defaultSizes[(int) TileSize.MEDIUM];
            checkboxWide.IsChecked = defaultSizes[(int) TileSize.WIDE];
            checkboxLarge.IsChecked = defaultSizes[(int) TileSize.LARGE];

            var oldImageLocation = oldImage?.GetFileStreamSource()?.Directory?.FullName;
            if (oldImageLocation != null)
                dialog.AddPlace(oldImageLocation, FileDialogAddPlaceLocation.Top);

            var result = dialog.ShowDialog(window);

            if (result != CommonFileDialogResult.Ok)
                return (null, null);

            var selection = new[]
            {
                checkboxSmall.IsChecked, 
                checkboxMedium.IsChecked,
                checkboxWide.IsChecked,
                checkboxLarge.IsChecked
            };
            return selection.All(b => false == b) ? (null, null) : (new FileInfo(dialog.FileName), selection);
        }

        public async Task Save()
        {
            Tile.BackgroundColorXml = BackgroundColor;
            Tile.ForegroundTextXml = ForegroundText;
            Tile.ShowNameOnSquare150x150Logo = ShowNameOnSquare150x150Logo;
            Tile.ShowNameOnWide310x150Logo = ShowNameOnWide310x150Logo;
            Tile.ShowNameOnSquare310x310Logo = ShowNameOnSquare310x310Logo;
            
            if (!ReferenceEquals(Tile.Square70x70Logo, Square70x70Logo))
            {
                Tile.Square70x70Logo = SaveImage(TileSize.SMALL, Square70x70Logo.GetFileStreamSource());
            }

            if (!ReferenceEquals(Tile.Square150x150Logo, Square150x150Logo))
            {
                Tile.Square150x150Logo = SaveImage(TileSize.MEDIUM, Square150x150Logo.GetFileStreamSource());
            }

            if (!ReferenceEquals(Tile.Wide310x150Logo, Wide310x150Logo))
            {
                Tile.Wide310x150Logo = SaveImage(TileSize.WIDE, Wide310x150Logo.GetFileStreamSource());
            }

            if (!ReferenceEquals(Tile.Square310x310Logo, Square310x310Logo))
            {
                Tile.Square310x310Logo = SaveImage(TileSize.LARGE, Square310x310Logo.GetFileStreamSource());
            }

            await Tile.Save();
        }
        
        private BitmapImage SaveImage(TileSize size, FileInfo file)
        {
            var rootFolder = SecondaryManager.GetTileFolder(Tile.TileID);

            var filename = StartmenuSecondaryHelperTile.GetFileNameFromSize(size);

            var copy = file.CopyTo(rootFolder.File(filename).FullName, true);

            return BitmapExtension.GetBitmapImageInMemory(copy);
        }

        public void ClearImages()
        {
            throw new InvalidOperationException();
        }
    }
}