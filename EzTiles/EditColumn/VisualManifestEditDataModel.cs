﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using EzTiles.Data;
using EzTiles.Properties;
using EzTiles.Util;
using IronStone.Moldinium;
using Microsoft.WindowsAPICodePack.Dialogs;
using Microsoft.WindowsAPICodePack.Dialogs.Controls;
using Microsoft.WindowsAPICodePack.Shell;
using Color = System.Windows.Media.Color;

namespace EzTiles
{
    public abstract class VisualManifestEditDataModel : IModel, EditDataModel
    {
        public LinkFileInfo _link;
        
        public string DisplayName { get; set; }

        public bool LockImages { get; set; }
        public bool IsSecondaryTile => false;
        public bool CanClearImages => !LockImages;

        public abstract Color BackgroundColor { get; set; }

        public BitmapImage Wide310x150Logo { get{return null;} set{throw new InvalidOperationException();} }
        public bool ShowNameOnWide310x150Logo { get{return false;} set{throw new InvalidOperationException();} }
        public BitmapImage Square310x310Logo { get{return null;} set{throw new InvalidOperationException();} }
        public bool ShowNameOnSquare310x310Logo  { get{return false;} set{throw new InvalidOperationException();} }

        public abstract bool ForegroundText { get; set; }

        public abstract BitmapImage Square70x70Logo { get; set; }
        public abstract BitmapSource Square70x70LogoFallback { get; set; }

        public abstract BitmapImage Square150x150Logo { get; set; }
        public abstract BitmapSource Square150x150LogoFallback { get; set; }
        public abstract bool ShowNameOnSquare150x150Logo { get; set; }

        public virtual bool IsStateValid => Square70x70Logo == null && Square150x150Logo == null ||
                                            Square70x70Logo != null && Square150x150Logo != null;

        public void ChooseImage(Window dialog, TileSize tileSize)
        {
            BitmapImage oldImage;
            switch (tileSize)
            {
                case TileSize.SMALL:
                    oldImage = Square70x70Logo;
                    break;
                case TileSize.MEDIUM:
                    oldImage = Square150x150Logo;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(tileSize), tileSize, null);
            }
            
            var (file, selection) = OpenImageChooser(dialog, DisplayName, GenArgs(tileSize), oldImage);
            if (file == null) return;

            if (selection[(int) TileSize.SMALL])
            {
                var image = BitmapExtension.GetBitmapImageInMemory(file);
                Square70x70Logo = image;
            }

            if (selection[(int) TileSize.MEDIUM])
            {
                var image = BitmapExtension.GetBitmapImageInMemory(file);
                Square150x150Logo = image;
            }
        }

        private (FileInfo, bool[]) OpenImageChooser(Window window, string title, bool[] defaultSizes,
            BitmapImage oldImage
            //,string initialDirectory = null, string additionalFilePlace = null
            )
        {
//            if (initialDirectory == null)
//            {
            var initialDirectory = Directory.Exists(Settings.Default.Images_Location)
                ? Settings.Default.Images_Location
                : @"c:\";
//            }

            var checkboxSmall = new CommonFileDialogCheckBox("Small (1x1)");
            var checkboxMedium = new CommonFileDialogCheckBox("Medium (2x2)");

            var dialog = new CommonOpenFileDialog
            {
                Title = title,
                InitialDirectory = initialDirectory,
                Filters = {new CommonFileDialogFilter("Image files", "*.jpeg;*.jpg;*.png")},
                EnsureFileExists = true,
                Controls =
                {
                    checkboxMedium,
                    checkboxSmall,
                }
            };

            checkboxSmall.IsChecked = defaultSizes[(int) TileSize.SMALL];
            checkboxMedium.IsChecked = defaultSizes[(int) TileSize.MEDIUM];

            dialog.AddPlace(((FileInfo) _link.Target.FileOrFolder).Directory.FullName, FileDialogAddPlaceLocation.Top);

            var oldImageLocation = oldImage?.GetFileStreamSource()?.Directory?.FullName;
            if (oldImageLocation != null)
                dialog.AddPlace(oldImageLocation, FileDialogAddPlaceLocation.Top);

            var result = dialog.ShowDialog(window);

            if (result != CommonFileDialogResult.Ok)
                return (null, null);

            var selection = new[] {checkboxSmall.IsChecked, checkboxMedium.IsChecked};
            return selection.All(b => false == b) ? (null, null) : (new FileInfo(dialog.FileName), selection);
        }
        
        private static bool[] GenArgs(params TileSize[] sizes)
        {
            var arr = new bool[4];
            foreach (var size in sizes)
            {
                arr[(int) size] = true;
            }

            return arr;
        }

        private BitmapImage SaveImage(string fileName, FileInfo file)
        {
            var rootFolder = ((FileInfo) _link.Target.FileOrFolder).Directory;

            if (file.EnumerateParents().Any(parent =>
                string.Equals(parent.FullName, rootFolder.FullName, StringComparison.CurrentCultureIgnoreCase)))
            {
                return BitmapExtension.GetBitmapImageInMemory(file);
            }
            // ReSharper disable once RedundantIfElseBlock
            else
            {
                var targetFolder = rootFolder.CreateSubdirectory("EzTilesImages");
                var targetFile = file.CopyTo(targetFolder.File(fileName + file.Extension).FullName, true);
                var image = BitmapExtension.GetBitmapImageInMemory(targetFile);
                return image;
            }
        }
        
        public void ClearImages()
        {
            Square70x70Logo = null;
            Square150x150Logo = null;
        }
        
        public async Task Save()
        {
            if (!IsStateValid)
            {
                throw new InvalidOperationException();
            }

            var manifest = _link.Target.VisualManifest;

            manifest.BackgroundColor = BackgroundColor;
            manifest.ForegroundText = ForegroundText;
            manifest.ShowNameOnSquare150x150Logo = ShowNameOnSquare150x150Logo;

            if (!ReferenceEquals(manifest.Square70x70Logo, Square70x70Logo))
            {
                manifest.Square70x70LogoResourcePath = null;
                manifest.Square70x70Logo = SaveImage("small_image", Square70x70Logo.GetFileStreamSource());
            }

            if (!ReferenceEquals(manifest.Square150x150Logo, Square150x150Logo))
            {
                manifest.Square150x150LogoResourcePath = null;
                manifest.Square150x150Logo = SaveImage("medium_image", Square150x150Logo.GetFileStreamSource());
            }

            await manifest.Save();

            _link.File.LastWriteTime = DateTime.Now;
        }
    }
}