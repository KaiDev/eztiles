﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;

namespace EzTiles_Common
{
    public static class HelperAction
    {
        public static int Handle(string[] split, IHelperHandler handler)
        {
            switch (split[0])
            {
                case nameof(IHelperHandler.CreateNewSecondary):
                    return handler.CreateNewSecondary(split[1], split[2], split[3], split[4], split[5], split[6]).Result;
                case nameof(IHelperHandler.GetShowNameOnSquare310x310Logo):
                    return handler.GetShowNameOnSquare310x310Logo(split[1]);
                case nameof(IHelperHandler.UpdateTile):
                    return handler.UpdateTile(split[1], split[2], split[3], split[4], split[5], split[6]).Result;
                default:
                    return handler.Default(split);
            }
        }

        public const string FILE_NAME_SMALL_IMAGE = "Square70x70Logo";
        public const string FILE_NAME_MEDIUM_IMAGE = "Square150x150Logo";
        public const string FILE_NAME_WIDE_IMAGE = "Wide310x150Logo";
        public const string FILE_NAME_LARGE_IMAGE = "Square310x310Logo";

        //CREDIT https://stackoverflow.com/questions/5510343/escape-command-line-arguments-in-c-sharp/6040946
        public static string EscapeSingleArgument(string s)
        {
            s = Regex.Replace(s, @"(\\*)""", @"$1$1\""");
            s = "\"" + Regex.Replace(s, @"(\\+)$", @"$1$1") + "\"";
            return s;
        }

        public static string ComposeArguments(params string[] args)
        {
            var str = string.Join(" ", args.Select(EscapeSingleArgument));
            return str;
        }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public interface IHelperHandler
    {
        Task<int> CreateNewSecondary(
            string guid, 
            string name, 
            string args, 
            string backgroundColor,
            string foregroundText,
            string showNameOnSquare150X150Logo);
        
        int GetShowNameOnSquare310x310Logo(string tileId);

        Task<int> UpdateTile(string tileId,
            string backgroundColor,
            string foregroundText,
            string showNameOnSquare150X150Logo,
            string showNameOnWide310x150Logo,
            string showNameOnSquare310x310Logo);
        
        int Default(string[] split);
    }
}