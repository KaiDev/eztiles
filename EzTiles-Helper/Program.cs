﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using Windows.Storage;
using Windows.UI;
using Windows.UI.StartScreen;
using EzTiles_Common;
using EzTiles_Helper_WPF;
using IWshRuntimeLibrary;
using Shell32;
using Condition = System.Windows.Automation.Condition;

namespace EzTiles_Helper
{
    class Program : IHelperHandler
    {
        public static int Main(string[] args)
        {
            //SpinWait.SpinUntil(() => Debugger.IsAttached);
            switch (args.Length)
            {
                case 0:
                    return 0;
                case 1:
                    Process.Start(new ProcessStartInfo()
                    {
                        FileName = args[0],
                        ErrorDialog = true,
                        UseShellExecute = true,
                    });
                    return 0;
                default:
                    return HelperAction.Handle(args, new Program());
            }
        }

        public async Task<int> CreateNewSecondary(
            string guid, 
            string name, 
            string args, 
            string backgroundColor,
            string foregroundText,
            string showNameOnSquare150X150Logo)
        {
            var targetFolder = await ApplicationData.Current.LocalFolder
                .CreateFolderAsync(guid, CreationCollisionOption.OpenIfExists);

            Uri GetFullImageFileName(string filename)
            {
                return new Uri($"ms-appdata:///local/{targetFolder.Name}/{filename}");
            }
            
            var tile = new SecondaryTile
            {
                TileId = guid,
                DisplayName = name,
                Arguments = args,
                VisualElements =
                {
                    ShowNameOnSquare150x150Logo = !string.IsNullOrEmpty(showNameOnSquare150X150Logo) && bool.Parse(showNameOnSquare150X150Logo),
                    ForegroundText = string.IsNullOrEmpty(foregroundText) ? 
                        ForegroundText.Light :
                        foregroundText == "light" ? ForegroundText.Light : 
                        foregroundText == "dark" ? ForegroundText.Dark : 
                            throw new Exception(),
                    BackgroundColor = string.IsNullOrEmpty(backgroundColor) ? 
                        Colors.Transparent :
                        ToColor(backgroundColor),
                    Square70x70Logo = GetFullImageFileName(HelperAction.FILE_NAME_SMALL_IMAGE),
                    Square150x150Logo = GetFullImageFileName(HelperAction.FILE_NAME_MEDIUM_IMAGE),
                    Wide310x150Logo = GetFullImageFileName(HelperAction.FILE_NAME_WIDE_IMAGE),
                    Square310x310Logo = GetFullImageFileName(HelperAction.FILE_NAME_LARGE_IMAGE),
                    ShowNameOnSquare310x310Logo = false,
                    ShowNameOnWide310x150Logo = false
                }
            };
            
            var initWindow = (IInitializeWithWindow) (object) tile;
            var desktop = GetDesktopWindow();
            initWindow.Initialize(desktop);

            var resTask = tile.RequestCreateAsync();

            var handle = IntPtr.Zero;
            var count = 0;
            
            while (handle == IntPtr.Zero &&
                   count < 10)
            {
                var temp = FindWindow("Shell_Dialog", null);
                var _ = GetWindowThreadProcessId(temp, out var processId);
                var process = Process.GetProcessById((int) processId);
                if (process.ProcessName.ToLower() == "pickerhost")
                {
                    handle = temp;
                }
                count++;
                Thread.Sleep(50);
            }

            if (handle != IntPtr.Zero)
            {
                var children = AutomationElement.FromHandle(handle)
                    .FindAll(TreeScope.Descendants, Condition.TrueCondition)
                    .OfType<AutomationElement>();
                var yesButton = children.Single(el => el.Current.AutomationId == "Button1");
                var pattern = (InvokePattern)yesButton.GetCurrentPattern(InvokePattern.Pattern);
                pattern.Invoke();
            }
            
            var res = await resTask;
            return res ? 0 : 1;
        }

        public static Color ToColor(string colorString)
        {
            if (string.IsNullOrEmpty(colorString))
            {
                throw new ArgumentException(nameof(colorString));
            }

            if (colorString[0] == '#')
            {
                switch (colorString.Length)
                {
                    case 9:
                        {
                            var cuint = Convert.ToUInt32(colorString.Substring(1), 16);
                            var a = (byte)(cuint >> 24);
                            var r = (byte)((cuint >> 16) & 0xff);
                            var g = (byte)((cuint >> 8) & 0xff);
                            var b = (byte)(cuint & 0xff);

                            return Color.FromArgb(a, r, g, b);
                        }
                    case 7:
                        {
                            var cuint = Convert.ToUInt32(colorString.Substring(1), 16);
                            var r = (byte)((cuint >> 16) & 0xff);
                            var g = (byte)((cuint >> 8) & 0xff);
                            var b = (byte)(cuint & 0xff);

                            return Color.FromArgb(255, r, g, b);
                        }
                    case 5:
                        {
                            var cuint = Convert.ToUInt16(colorString.Substring(1), 16);
                            var a = (byte)(cuint >> 12);
                            var r = (byte)((cuint >> 8) & 0xf);
                            var g = (byte)((cuint >> 4) & 0xf);
                            var b = (byte)(cuint & 0xf);
                            a = (byte)(a << 4 | a);
                            r = (byte)(r << 4 | r);
                            g = (byte)(g << 4 | g);
                            b = (byte)(b << 4 | b);

                            return Color.FromArgb(a, r, g, b);
                        }
                    case 4:
                        {
                            var cuint = Convert.ToUInt16(colorString.Substring(1), 16);
                            var r = (byte)((cuint >> 8) & 0xf);
                            var g = (byte)((cuint >> 4) & 0xf);
                            var b = (byte)(cuint & 0xf);
                            r = (byte)(r << 4 | r);
                            g = (byte)(g << 4 | g);
                            b = (byte)(b << 4 | b);

                            return Color.FromArgb(255, r, g, b);
                        }
                    default:
                        throw new FormatException(
                            $"The {colorString} string passed in the colorString argument is not a recognized Color format.");
                }
            }

            if (colorString.Length > 3 && colorString[0] == 's' && colorString[1] == 'c' && colorString[2] == '#')
            {
                var values = colorString.Split(',');

                switch (values.Length)
                {
                    case 4:
                    {
                        var scA = double.Parse(values[0].Substring(3));
                        var scR = double.Parse(values[1]);
                        var scG = double.Parse(values[2]);
                        var scB = double.Parse(values[3]);

                        return Color.FromArgb((byte)(scA * 255), (byte)(scR * 255), (byte)(scG * 255), (byte)(scB * 255));
                    }
                    case 3:
                    {
                        var scR = double.Parse(values[0].Substring(3));
                        var scG = double.Parse(values[1]);
                        var scB = double.Parse(values[2]);

                        return Color.FromArgb(255, (byte)(scR * 255), (byte)(scG * 255), (byte)(scB * 255));
                    }
                }

                throw new FormatException(
                    $"The {colorString} string passed in the colorString argument is not a recognized Color format (sc#[scA,]scR,scG,scB).");
            }

            var prop = typeof(Colors).GetTypeInfo().GetDeclaredProperty(colorString);

            if (prop != null)
            {
                return (Color)prop.GetValue(null);
            }

            throw new FormatException(string.Format("The {0} string passed in the colorString argument is not a recognized Color.", colorString));
        }

        public int GetShowNameOnSquare310x310Logo(string tileId)
        {
            var tiles = SecondaryTile.FindAllAsync().AsTask().Result;
            var tile = tiles.Single(t => t.TileId == tileId);
            return tile.VisualElements.ShowNameOnSquare310x310Logo ? 1 : 0;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public async Task<int> UpdateTile(string tileId,
            string backgroundColor,
            string foregroundText,
            string showNameOnSquare150X150Logo,
            string showNameOnWide310x150Logo,
            string showNameOnSquare310x310Logo)
        {
            var tileList = await SecondaryTile.FindAllAsync();
            var tile = tileList.Single(t => t.TileId == tileId);

            tile.VisualElements.BackgroundColor = ToColor(backgroundColor);
            tile.VisualElements.ForegroundText = 
                foregroundText == "light" ? ForegroundText.Light :
                foregroundText == "dark" ? ForegroundText.Dark : throw new Exception();
            tile.VisualElements.ShowNameOnSquare150x150Logo = bool.Parse(showNameOnSquare150X150Logo);
            tile.VisualElements.ShowNameOnWide310x150Logo = bool.Parse(showNameOnWide310x150Logo);
            tile.VisualElements.ShowNameOnSquare310x310Logo = bool.Parse(showNameOnSquare310x310Logo);
            
            var res = await tile.UpdateAsync();
            return res ? 0 : 1;
        }
        
        [DllImport("User32.dll")]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll", SetLastError = false)]
        private static extern IntPtr GetDesktopWindow();
        
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        
        public int Default(string[] split)
        {
            MessageBox.Show(string.Join("\n", split));
            return 0;
        }
    }
}